const colours = require("tailwindcss/colors");

module.exports = {
	purge: {
		content: ["./src/pages/**/*.{js,ts,jsx,tsx}", "./src/components/**/*.{js,ts,jsx,tsx}"],
		options: {
			safelist: [
				/^bg-pink-/,
				/^bg-red-/,
				/^bg-yellow-/,
				/^bg-green-/,
				/^bg-indigo-/,
				/^border-green-/,
				/^border-red-/,
				/^border-cyan-/,
				/^text-green-/,
				/^text-red-/,
				/^text-cyan-/
			]
		}
	},
	darkMode: "class",
	theme: {
		extend: {
			colors: {
				dark: {
					100: "#808080",
					200: "#707070",
					300: "#606060",
					400: "#505050",
					500: "#404040",
					600: "#303030",
					650: "#252525",
					700: "#202020",
					750: "#151515",
					800: "#101010",
					900: "#000000"
				},
				spotify: {
					DEFAULT: "#1DB954"
				},
				sky: colours.sky,
				cyan: colours.cyan,
				orange: colours.orange
			},
			spacing: {
				33: "8.25rem"
			},
			cursor: {
				"zoom-in": "zoom-in"
			}
		}
	},
	variants: {
		extend: {
			opacity: ["disabled"],
			cursor: ["disabled"],
			backgroundColor: ["disabled"]
		}
	},
	plugins: [
		require("@tailwindcss/forms")
	]
};

