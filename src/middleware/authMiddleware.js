import AuthTokenSchema from "../schemas/API/AuthTokenSchema";
import {verifyJwtAsync} from "../helpers/auth";
import {internalServerError} from "../lib/responses";

const authMiddleware = (handler, ignoredMethods = []) => async (req, res) => {
	if (ignoredMethods.includes(req.method))
		return handler(req, res);

	let {cookies} = req;

	let validationResult = AuthTokenSchema.validate(cookies, {
		abortEarly: false,
		stripUnknown: true
	});

	if (validationResult.error) {
		return res.status(401).send({
			error: true,
			displayMessage: "There are validation errors within the cookies",
			errors: validationResult.error.details,
			code: "COOKIES_VALIDATION_ERROR"
		});
	}

	let {
		token
	} = validationResult.value;

	try {
		let payload = await verifyJwtAsync(token);
		req.userId = payload.userId;
	} catch (err) {
		if (err.message === "jwt expired" || err.message === "invalid signature"
			|| err.message === "invalid token") {
			return res.status(401).send({
				error: true,
				displayMessage: "The provided authentication token is invalid",
				code: "INVALID_TOKEN"
			});
		}

		if (err.message === "jwt malformed") {
			return res.status(401).send({
				error: true,
				displayMessage: "The provided authentication token is malformed",
				code: "INVALID_TOKEN"
			});
		}

		return res.status(500).send(internalServerError);
	}

	return handler(req, res);
};

export default authMiddleware;
