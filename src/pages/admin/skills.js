import React, {useEffect} from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import AdminSkillsList from "../../components/Admin/Skills/AdminSkillsList";
import {useRouter} from "next/router";
import {useAlert} from "react-alert";
import {useAuth} from "../../lib/useAuth";
import ErrorPage from "../../components/Errors/ErrorPage";

const Skills = ({error}) => {
	const router = useRouter();
	const alert = useAlert();

	if (error)
		return <ErrorPage statusCode={error}/>;

	useEffect(() => {
		let {addedSkill, editedSkill} = router.query;

		if (addedSkill === "1") {
			alert.success("Successfully added new skill");
			window.history.replaceState(null, null, router.pathname);
		}

		if (editedSkill === "1") {
			alert.success("Successfully edited skill");
			window.history.replaceState(null, null, router.pathname);
		}
	}, []);

	return (
		<>
			<SmallHeader
				title="Manage Skills"
			/>

			<AdminSkillsList/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default Skills;
