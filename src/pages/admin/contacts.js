import React, {useEffect} from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import AdminContactsList from "../../components/Admin/Contacts/AdminContactsList";
import {useRouter} from "next/router";
import {useAlert} from "react-alert";
import {useAuth} from "../../lib/useAuth";
import ErrorPage from "../../components/Errors/ErrorPage";

const Contacts = ({error}) => {
	const router = useRouter();
	const alert = useAlert();

	if (error)
		return <ErrorPage statusCode={error}/>;

	useEffect(() => {
		let {addedContact} = router.query;

		if (addedContact === "1") {
			alert.success("Successfully added new contact");
			window.history.replaceState(null, null, router.pathname);
		}
	}, []);

	return (
		<>
			<SmallHeader
				title="Manage Contacts"
			/>

			<AdminContactsList/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default Contacts;
