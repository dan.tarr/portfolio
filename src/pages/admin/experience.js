import React from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import AdminExperienceList from "../../components/Admin/Experience/AdminExperienceList";
import {useAuth} from "../../lib/useAuth";
import ErrorPage from "../../components/Errors/ErrorPage";

const Experience = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return (
		<>
			<SmallHeader
				title="Manage Experiences"
			/>

			<AdminExperienceList/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default Experience;
