import React from "react";
import SmallHeader from "../../../components/Header/SmallHeader";
import NewEducation from "../../../components/Admin/Education/NewEducation";
import {useAuth} from "../../../lib/useAuth";
import ErrorPage from "../../../components/Errors/ErrorPage";

const New = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return (
		<>
			<SmallHeader
				title="Create New Education"
			/>

			<NewEducation/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default New;
