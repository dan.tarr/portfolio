import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import LoginForm from "../components/Form/LoginForm";

const Login = () => {
	return (
		<>
			<SmallHeader
				title="Login"
				subtitle="Sign into your account to manage the content displayed on the site"
			/>

			<LoginForm/>
		</>
	);
};

export default Login;
