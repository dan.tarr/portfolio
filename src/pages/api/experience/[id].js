import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	notFound,
	methodNotAllowed
} from "../../../lib/responses";
import authMiddleware from "../../../middleware/authMiddleware";
import EditExperienceSchema from "../../../schemas/API/EditExperienceSchema";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let {id} = req.query;

			let experienceQuery = queryBuilder.table("experience")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [experience] = await query(experienceQuery.sql, experienceQuery.bindings);

			if (!experience)
				return res.status(404).send(notFound("Experience"));

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				experience
			});
		}

		if (req.method === "PUT") {
			let {body} = req;
			let {id} = req.query;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let experienceQuery = queryBuilder.table("experience")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [experience] = await query(experienceQuery.sql, experienceQuery.bindings);

			if (!experience)
				return res.status(404).send(notFound("Experience"));

			let validationResult = EditExperienceSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				title,
				description,
				company,
				url,
				iconUrl,
				startDate,
				endDate
			} = validationResult.value;

			experience.title = title;
			experience.description = description;
			experience.company = company;
			experience.url = url;
			experience.iconUrl = iconUrl;
			experience.startDate = startDate;
			experience.endDate = endDate;

			let updateQuery = queryBuilder.table("experience")
				.update(experience)
				.where({
					id
				})
				.toSQL();

			await query(updateQuery.sql, updateQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				experience
			});
		}

		if (req.method === "DELETE") {
			let {id} = req.query;

			let experienceQuery = queryBuilder.table("experience")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [experience] = await query(experienceQuery.sql, experienceQuery.bindings);

			if (!experience)
				return res.status(404).send(notFound("Experience"));

			let deleteQuery = queryBuilder.table("experience")
				.del()
				.where({
					id
				})
				.toSQL();

			await query(deleteQuery.sql, deleteQuery.bindings);

			return res.status(204).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET", "PUT", "DELETE"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
