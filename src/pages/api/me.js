import {
	internalServerError,
	methodNotAllowed
} from "../../lib/responses";
import {
	query,
	queryBuilder
} from "../../lib/database";
import {verifyJwtAsync} from "../../helpers/auth";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let {
				token,
				refreshToken
			} = req.cookies;

			let tokenPayload;

			try {
				tokenPayload = await verifyJwtAsync(token, true);
				await verifyJwtAsync(refreshToken);
			} catch (err) {
				return res.status(401).send({
					error: true,
					displayMessage: "The provided authentication token or refresh token is invalid",
					code: "INVALID_TOKEN"
				});
			}

			let userQuery = queryBuilder.table("users")
				.select("id", "username", "avatarUrl", "webauthnLoginEnabled")
				.where({
					id: tokenPayload.userId
				})
				.toSQL();

			let [user] = await query(userQuery.sql, userQuery.bindings);

			// Convert 1 or 0 to true or false
			user.webauthnLoginEnabled = user.webauthnLoginEnabled === 1;

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				user
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
