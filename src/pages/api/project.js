import {
	internalServerError,
	methodNotAllowed
} from "../../lib/responses";
import {
	query,
	queryBuilder
} from "../../lib/database";
import CreateProjectSchema from "../../schemas/API/CreateProjectSchema";
import authMiddleware from "../../middleware/authMiddleware";
import {generateId} from "../../helpers/general";
import ListProjectsQueryParamsSchema from "../../schemas/API/ListProjectsQueryParamsSchema";

const perPage = 12;

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let {query: queryParams} = req;

			let validationResult = ListProjectsQueryParamsSchema.validate(queryParams, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the query parameters",
					errors: validationResult.error.details,
					code: "QUERY_VALIDATION_ERROR"
				});
			}

			let {
				page,
				sort,
				sortDirection,
				filterType,
				filterValue,
				skillId
			} = validationResult.value;

			let offsetValue = (page - 1) * perPage;

			let resultsQuery = queryBuilder.table("projects")
				.select(["projects.*"])
				.orderBy(sort, sortDirection)
				.limit(perPage)
				.offset(offsetValue);

			let countQuery = queryBuilder.table("projects")
				.count("projects.id", {as: "count"});

			if (skillId) {
				resultsQuery.leftJoin("projectSkills",
					"projects.id", "projectSkills.projectId")
					.groupBy("projects.id")
					.where("projectSkills.skillId", skillId);
				countQuery.leftJoin("projectSkills",
					"projects.id", "projectSkills.projectId")
					.where("projectSkills.skillId", skillId);
			}

			if (filterType) {
				resultsQuery.where({
					[filterType]: filterValue
				});
				countQuery.where({
					[filterType]: filterValue
				});
			}

			resultsQuery = resultsQuery.toSQL();
			countQuery = countQuery.toSQL();

			let [projects, [totalResults]] = await Promise.all([
				query(resultsQuery.sql, resultsQuery.bindings),
				query(countQuery.sql, countQuery.bindings)
			]);

			let projectIds = projects.map(project => project.id);

			let projectSkillsQuery = queryBuilder.table("projectSkills")
				.select("*")
				.whereIn("projectId", projectIds)
				.toSQL();

			let projectSkills = await query(projectSkillsQuery.sql, projectSkillsQuery.bindings);

			for (let project of projects) {
				project.openSource = project.openSource === 1;
				project.contributors = JSON.parse(project.contributors);
				project.skills = projectSkills.filter(projectSkill =>
					projectSkill.projectId === project.id);
				project.imageUrls = project.imageUrls.split(",");
			}

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				projects,
				pageInfo: {
					totalResults: totalResults.count,
					page,
					totalPages: Math.ceil(totalResults.count / perPage),
					perPage
				}
			});
		}

		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = CreateProjectSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				name,
				shortDescription,
				longDescription,
				openSource,
				repository,
				contributors,
				imageUrls,
				skillIds
			} = validationResult.value;

			if (repository) {
				let repoUrlObject = new URL(repository);

				if (repoUrlObject.host !== "gitlab.com" && repoUrlObject.host !== "github.com") {
					return res.status(400).send({
						error: false,
						displayMessage: "There are validation errors within the body",
						errors: [
							{
								message: "\"repository\" must be a gitlab.com or github.com URL",
								path: [
									"repository"
								],
								type: "string.uriCustomScheme",
								context: {
									label: "repository",
									value: repository,
									key: "repository"
								}
							}
						],
						code: "BODY_VALIDATION_ERROR"
					});
				}
			}

			let existingProjectQuery = queryBuilder.table("projects")
				.select("id")
				.where({
					name
				})
				.toSQL();

			let [existingProject] = await query(existingProjectQuery.sql, existingProjectQuery.bindings);

			if (existingProject) {
				return res.status(409).send({
					error: true,
					displayMessage: "A project with that name already exists",
					code: "PROJECT_EXISTS"
				});
			}

			let project = {
				id: generateId("proj"),
				name,
				shortDescription,
				longDescription,
				openSource,
				repository,
				contributors,
				imageUrls,
				dateAdded: new Date().getTime()
			};

			let insertSkillsQuery;

			if (skillIds.length > 0) {
				let skillsQuery = queryBuilder.table("skills")
					.select("*")
					.whereIn("id", skillIds)
					.toSQL();

				let skills = await query(skillsQuery.sql, skillsQuery.bindings);

				if (skills.length !== skillIds.length) {
					return res.status(400).send({
						error: true,
						displayMessage: "There are validation errors within the body",
						errors: [{
							message: "\"skillIds\" must be an array of valid IDs",
							path: ["skillIds"]
						}],
						code: "BODY_VALIDATION_ERROR"
					});
				}

				let projectSkills = skills.map(skill => {
					return {
						id: generateId("projSkill"),
						projectId: project.id,
						skillId: skill.id
					};
				});

				insertSkillsQuery = queryBuilder.table("projectSkills")
					.insert(projectSkills)
					.toSQL();

				project.skills = projectSkills;
			}

			let insertQuery = queryBuilder.table("projects")
				.insert({
					id: project.id,
					name: project.name,
					shortDescription: project.shortDescription,
					longDescription: project.longDescription,
					openSource: project.openSource,
					repository: project.repository,
					contributors: JSON.stringify(project.contributors),
					imageUrls: project.imageUrls.join(","),
					dateAdded: project.dateAdded
				})
				.toSQL();

			await query(insertQuery.sql, insertQuery.bindings);

			if (insertSkillsQuery)
				await query(insertSkillsQuery.sql, insertSkillsQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				project
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
