import {internalServerError, methodNotAllowed} from "../../lib/responses";
import {getCalendarStatistics, getLastContributionTimestamp} from "../../helpers/gitlab";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let statsObject = await getCalendarStatistics();
			let lastContributionTimestamp = await getLastContributionTimestamp();

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				data: {
					...statsObject,
					lastContributionTimestamp
				}
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
