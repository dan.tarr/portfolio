import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	notFound,
	methodNotAllowed
} from "../../../lib/responses";
import EditProjectSchema from "../../../schemas/API/EditProjectSchema";
import authMiddleware from "../../../middleware/authMiddleware";
import {generateId} from "../../../helpers/general";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let {id} = req.query;

			let projectQuery = queryBuilder.table("projects")
				.select("*")
				.where({
					id
				})
				.orWhere({
					name: id
				})
				.toSQL();

			let [project] = await query(projectQuery.sql, projectQuery.bindings);

			if (!project)
				return res.status(404).send(notFound("Project"));

			let projectSkillsQuery = queryBuilder.table("projectSkills")
				.select("*")
				.where({
					projectId: id
				})
				.toSQL();

			project.openSource = project.openSource === 1;
			project.contributors = JSON.parse(project.contributors);
			project.skills = await query(projectSkillsQuery.sql, projectSkillsQuery.bindings);
			project.imageUrls = project.imageUrls.split(",");

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				project
			});
		}

		if (req.method === "PUT") {
			let {body} = req;
			let {id} = req.query;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let projectQuery = queryBuilder.table("projects")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [project] = await query(projectQuery.sql, projectQuery.bindings);

			if (!project)
				return res.status(404).send(notFound("Project"));

			let validationResult = EditProjectSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				name,
				shortDescription,
				longDescription,
				openSource,
				repository,
				contributors,
				imageUrls,
				skillIds
			} = validationResult.value;

			project.name = name;
			project.shortDescription = shortDescription;
			project.longDescription = longDescription;
			project.openSource = openSource;
			project.repository = repository;
			project.contributors = contributors;
			project.imageUrls = imageUrls;

			let projectSkillsQuery = queryBuilder.table("projectSkills")
				.select("*")
				.where({
					projectId: project.id
				})
				.toSQL();

			let projectSkills = await query(projectSkillsQuery.sql, projectSkillsQuery.bindings);

			let skillsToRemove = projectSkills.filter(projectSkill =>
				!skillIds.includes(projectSkill.skillId));
			let skillsToAdd = skillIds.filter(skill =>
				!projectSkills.find(projectSkill => projectSkill.skillId === skill));

			if (skillsToRemove.length > 0) {
				let removeSkillsQuery = queryBuilder.table("projectSkills")
					.del()
					.whereIn("id", skillsToRemove.map(skill => skill.id))
					.toSQL();

				// Remove the skills that were removed from the original project skills
				for (let i = 0; i < projectSkills.length + 1; i++) {
					if (skillsToRemove.includes(projectSkills[i]))
						projectSkills[i] = null;
				}

				// Remove null values
				projectSkills = projectSkills.filter(x => x);

				await query(removeSkillsQuery.sql, removeSkillsQuery.bindings);
			}

			if (skillsToAdd.length > 0) {
				let newProjectSkills = skillsToAdd.map(skillId => {
					return {
						id: generateId("projSkill"),
						projectId: project.id,
						skillId
					};
				});

				let addSkillsQuery = queryBuilder.table("projectSkills")
					.insert(newProjectSkills)
					.toSQL();

				projectSkills = projectSkills.concat(newProjectSkills);

				await query(addSkillsQuery.sql, addSkillsQuery.bindings);
			}

			let updateQuery = queryBuilder.table("projects")
				.update({
					id: project.id,
					name: project.name,
					shortDescription: project.shortDescription,
					longDescription: project.longDescription,
					openSource: project.openSource,
					repository: project.repository,
					contributors: JSON.stringify(project.contributors),
					imageUrls: project.imageUrls.join(",")
				})
				.where({
					id
				})
				.toSQL();

			project.skills = projectSkills;

			await query(updateQuery.sql, updateQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				project
			});
		}

		if (req.method === "DELETE") {
			let {id} = req.query;

			let projectQuery = queryBuilder.table("projects")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [project] = await query(projectQuery.sql, projectQuery.bindings);

			if (!project)
				return res.status(404).send(notFound("Project"));

			let deleteProjectSkillsQuery = queryBuilder.table("projectSkills")
				.del()
				.where({
					projectId: id
				})
				.toSQL();

			let deleteQuery = queryBuilder.table("projects")
				.del()
				.where({
					id
				})
				.toSQL();

			await query(deleteProjectSkillsQuery.sql, deleteProjectSkillsQuery.bindings);
			await query(deleteQuery.sql, deleteQuery.bindings);

			return res.status(204).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET", "PUT", "DELETE"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
