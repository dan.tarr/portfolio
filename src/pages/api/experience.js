import {
	internalServerError,
	methodNotAllowed
} from "../../lib/responses";
import {
	query,
	queryBuilder
} from "../../lib/database";
import authMiddleware from "../../middleware/authMiddleware";
import CreateExperienceSchema from "../../schemas/API/CreateExperienceSchema";
import {generateId} from "../../helpers/general";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let resultsQuery = queryBuilder.table("experience")
				.select("*")
				.orderByRaw("endDate IS NULL DESC, endDate DESC, startDate DESC")
				.toSQL();

			let results = await query(resultsQuery.sql, resultsQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				experience: results
			});
		}

		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = CreateExperienceSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				title,
				description,
				company,
				url,
				iconUrl,
				startDate,
				endDate
			} = validationResult.value;

			let experience = {
				id: generateId("exp"),
				title,
				description,
				company,
				url,
				iconUrl,
				startDate,
				endDate
			};

			let insertQuery = queryBuilder.table("experience")
				.insert(experience)
				.toSQL();

			await query(insertQuery.sql, insertQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				experience
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET", "POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
