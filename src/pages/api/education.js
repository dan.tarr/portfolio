import {
	internalServerError,
	methodNotAllowed
} from "../../lib/responses";
import {
	query,
	queryBuilder
} from "../../lib/database";
import authMiddleware from "../../middleware/authMiddleware";
import CreateEducationSchema from "../../schemas/API/CreateEducationSchema";
import {generateId} from "../../helpers/general";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let resultsQuery = queryBuilder.table("education")
				.select("*")
				.orderBy([
					{
						column: "endDate",
						order: "desc"
					},
					{
						column: "subject",
						order: "asc"
					}
				])
				.toSQL();

			let results = await query(resultsQuery.sql, resultsQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				education: results
			});
		}

		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = CreateEducationSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				location,
				level,
				subject,
				startDate,
				endDate,
				grade
			} = validationResult.value;

			let education = {
				id: generateId("edu"),
				location,
				level,
				subject,
				startDate,
				endDate,
				grade
			};

			let insertQuery = queryBuilder.table("education")
				.insert(education)
				.toSQL();

			await query(insertQuery.sql, insertQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				education
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET", "POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
