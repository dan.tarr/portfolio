import {
	query,
	queryBuilder
} from "../../../../lib/database";
import {internalServerError, methodNotAllowed} from "../../../../lib/responses";
import {generateAssertionChallenge} from "../../../../helpers/webauthn";
import LoginWebAuthnQueryParamsSchema from "../../../../schemas/API/LoginWebAuthnQueryParamsSchema";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let {query: queryParams} = req;

			let validationResult = LoginWebAuthnQueryParamsSchema.validate(queryParams, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the query parameters",
					errors: validationResult.error.details,
					code: "QUERY_VALIDATION_ERROR"
				});
			}

			let {
				username
			} = validationResult.value;

			let userQuery = queryBuilder.table("users")
				.select("id", "webauthnLoginEnabled", "webauthnAuthrInfo")
				.where({
					username
				})
				.toSQL();

			let [user] = await query(userQuery.sql, userQuery.bindings);

			if (!user) {
				return res.status(400).send({
					error: true,
					displayMessage: "Passwordless login has not been setup on this account, " +
						"or this user does not exist",
					code: "NOT_SUPPORTED"
				});
			}

			let {webauthnLoginEnabled, webauthnAuthrInfo} = user;

			if (!webauthnLoginEnabled || !webauthnAuthrInfo) {
				return res.status(400).send({
					error: true,
					displayMessage: "Passwordless login has not been setup on this account, " +
						"or this user does not exist",
					code: "NOT_SUPPORTED"
				});
			}

			webauthnAuthrInfo = JSON.parse(webauthnAuthrInfo);

			let serverChallenge = generateAssertionChallenge(webauthnAuthrInfo.credID);

			let userUpdateQuery = queryBuilder.table("users")
				.update({
					webauthnLoginChallenge: serverChallenge.challenge
				})
				.where({
					id: user.id
				})
				.toSQL();

			await query(userUpdateQuery.sql, userUpdateQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				challenge: serverChallenge
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
