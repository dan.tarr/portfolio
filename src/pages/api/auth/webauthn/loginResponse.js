import {
	query,
	queryBuilder
} from "../../../../lib/database";
import {internalServerError, methodNotAllowed} from "../../../../lib/responses";
import base64url from "base64url";
import {
	verifyAuthenticatorAssertionResponse
} from "../../../../helpers/webauthn";
import LoginResponseWebauthnSchema from "../../../../schemas/API/LoginResponseWebauthnSchema";
import {v4 as uuidv4} from "uuid";
import {signJwtAsync} from "../../../../helpers/auth";
import {serialize} from "cookie";

const handler = async (req, res) => {
	try {
		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = LoginResponseWebauthnSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				response,
				username
			} = validationResult.value;

			let userQuery = queryBuilder.table("users")
				.select("id", "webauthnLoginChallenge", "webauthnAuthrInfo")
				.where({
					username
				})
				.toSQL();

			let [user] = await query(userQuery.sql, userQuery.bindings);
			let userChallenge = user.webauthnLoginChallenge;

			let clientData = JSON.parse(base64url.decode(response.clientDataJSON));

			if (clientData.challenge !== userChallenge) {
				return res.status(400).send({
					error: true,
					displayMessage: "Challenges do not match",
					code: "CHALLENGE_MISMATCH"
				});
			}

			let result;
			let webauthnAuthrInfo = JSON.parse(user.webauthnAuthrInfo);

			try {
				if (response.authenticatorData) {
					result = await verifyAuthenticatorAssertionResponse(response, webauthnAuthrInfo);

					if (result.verified) {
						let updateUserQuery = queryBuilder.table("users")
							.update({
								webauthnLoginChallenge: null
							})
							.where({
								id: user.id
							})
							.toSQL();

						await query(updateUserQuery.sql, updateUserQuery.bindings);
					}
				} else {
					return res.status(422).send({
						error: true,
						displayMessage: "Failed to determine response type",
						code: "UNKNOWN_RESPONSE_TYPE"
					});
				}
			} catch (err) {
				return res.status(400).send({
					error: true,
					displayMessage: "Failed to authenticate",
					code: "AUTHENTICATION_FAILED"
				});
			}

			if (!result.verified) {
				return res.status(400).send({
					error: true,
					displayMessage: "Verification failed",
					code: "VERIFICATION_FAILED"
				});
			}

			let tokenId = uuidv4();
			let authToken = await signJwtAsync({
				userId: user.id,
				tokenId
			});
			let refreshToken = await signJwtAsync({
				tokenId
			}, "30d");

			let cookieExpiryDate = new Date();
			cookieExpiryDate.setDate(cookieExpiryDate.getDate() + 31);

			res.setHeader("Set-Cookie", [
				serialize("token", authToken, {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: cookieExpiryDate
				}),
				serialize("refreshToken", refreshToken, {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: cookieExpiryDate
				})
			]);

			return res.status(200).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
