import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	methodNotAllowed
} from "../../../lib/responses";
import LoginSchema from "../../../schemas/API/LoginSchema";
import {
	compareHash,
	signJwtAsync
} from "../../../helpers/auth";
import {serialize} from "cookie";
import {v4 as uuidv4} from "uuid";

const handler = async (req, res) => {
	try {
		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = LoginSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				username,
				password
			} = validationResult.value;

			let userQuery = queryBuilder.table("users")
				.select("*")
				.where({
					username
				})
				.toSQL();

			let [user] = await query(userQuery.sql, userQuery.bindings);

			if (!user) {
				return res.status(400).send({
					error: true,
					displayMessage: "Incorrect username or password",
					code: "INVALID_USERNAME_PASS"
				});
			}

			let validPassword = await compareHash(password, user.password);

			if (!validPassword) {
				return res.status(400).send({
					error: true,
					displayMessage: "Incorrect username or password",
					code: "INVALID_USERNAME_PASS"
				});
			}

			let tokenId = uuidv4();
			let authToken = await signJwtAsync({
				userId: user.id,
				tokenId
			});
			let refreshToken = await signJwtAsync({
				tokenId
			}, "30d");

			let cookieExpiryDate = new Date();
			cookieExpiryDate.setDate(cookieExpiryDate.getDate() + 31);

			res.setHeader("Set-Cookie", [
				serialize("token", authToken, {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: cookieExpiryDate
				}),
				serialize("refreshToken", refreshToken, {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: cookieExpiryDate
				})
			]);

			return res.status(200).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
