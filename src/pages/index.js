import React, {useEffect, useState} from "react";
import Header from "../components/Header/Header";
import SpotifyPlayer from "../components/Spotify/SpotifyPlayer";
import {DateTime} from "luxon";
import {useRouter} from "next/router";
import {useAlert} from "react-alert";

const Index = () => {
	const introductions = ["Hello", "Hey", "Hi"];
	const router = useRouter();
	const alert = useAlert();
	// I can't decide which "intro word" to use, so use them all randomly
	const intro = introductions[Math.floor(Math.random() * introductions.length)];
	const [age, setAge] = useState(null);

	useEffect(() => {
		let ageDuration = DateTime.fromObject({
			day: 28,
			month: 5,
			year: 2003
		}).diffNow("years");

		setAge(Math.floor(Math.abs(ageDuration.years)));

		let {loggedIn} = router.query;

		if (loggedIn === "1") {
			alert.success("Successfully logged in");
			window.history.replaceState(null, null, router.pathname);
		}
	}, []);

	const subtitle = <p>
		A {age} year old full stack-ish developer from South Wales, UK.

		<p className="mt-4 ml-2 text-lg text-gray-100">
			An aspiring software engineer often working
			in full stack on personal projects with a particular
			interest in backend API/microservice development.
		</p>

		<p className="mt-4 ml-2 text-lg text-gray-100">
			You can find a list of my projects <a className="text-blue-400" href="/projects">here</a>.
		</p>

		<p className="mt-4 ml-2 text-lg text-gray-100">
			Should you wish to contact me, you can find my contact details
			and social media <a className="text-blue-400" href="/contact">here</a>.
		</p>

		<h4 className="mt-20 ml-2 text-xl text-gray-100">
			This site is {" "}
			<a className="text-blue-400" href="https://gitlab.com/dan.tarr/portfolio" target="_blank">
				open source
			</a> {" "}
			on GitLab.
		</h4>
	</p>;

	return (
		<>
			<Header
				title={`${intro}, I'm Dan.`}
				subtitle={subtitle}
			/>

			<SpotifyPlayer/>
		</>
	);
};

export default Index;
