import {useEffect} from "react";
import {serialize} from "cookie";
import {useRouter} from "next/router";

const Logout = () => {
	const router = useRouter();

	useEffect(async () => {
		await router.push("/");
	}, []);

	return null;
};

export const getServerSideProps = (ctx) => {
	let date = new Date(0);

	ctx.res.setHeader("Set-Cookie", [
		serialize("token", "", {
			path: "/",
			domain: process.env.AUTH_COOKIE_DOMAIN,
			secure: true,
			httpOnly: true,
			expires: date
		}),
		serialize("refreshToken", "", {
			path: "/",
			domain: process.env.AUTH_COOKIE_DOMAIN,
			secure: true,
			httpOnly: true,
			expires: date
		})
	]);

	return {
		props: {}
	};
};

export default Logout;
