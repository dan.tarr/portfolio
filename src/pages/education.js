import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import EducationList from "../components/Education/EducationList";

const Education = () => {
	return (
		<>
			<SmallHeader
				title="Education"
			/>

			<EducationList/>
		</>
	);
};

export default Education;
