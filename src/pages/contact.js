import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import ContactList from "../components/Contact/ContactList";

const Contact = () => {
	return (
		<>
			<SmallHeader
				title="Contact"
				subtitle="Should you wish to contact me, you can find my contact details/social media below"
			/>

			<ContactList/>
		</>
	);
};

export default Contact;
