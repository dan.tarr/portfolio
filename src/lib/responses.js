export const internalServerError = {
	error: true,
	displayMessage: "An error occured",
	code: "INTERNAL_SERVER_ERROR"
};

export const notFound = (resourceName) => {
	return {
		error: true,
		displayMessage: `${resourceName} could not be found`,
		code: "RESOURCE_NOT_FOUND"
	};
};

export const methodNotAllowed = (methodUsed, allowedMethods) => {
	return {
		error: true,
		displayMessage: `${methodUsed} is not allowed on this endpoint`,
		code: "METHOD_NOT_ALLOWED",
		allowedMethods
	};
};
