import React, {useState} from "react";
import Button from "../../Button/Button";
import initialiseRegistration from
	"../../../helpers/apiMethods/auth/webauthn/initialiseRegistration";
import webauthnRegisterResponse from "../../../helpers/apiMethods/auth/webauthn/registerResponse";
import {useAlert} from "react-alert";
import {publicKeyCredentialToJson} from "../../../helpers/webauthn";
import useEndpoint from "../../../lib/useEndpoint";
import SubHeader from "../../Header/SubHeader";
import {ExclamationIcon} from "@heroicons/react/solid";
import Skeleton from "react-loading-skeleton";
import remove from "../../../helpers/apiMethods/auth/webauthn/remove";
import {mutate} from "swr";

const PasswordlessLogin = () => {
	const alert = useAlert();
	const {user, loading: userLoading} = useEndpoint("/api/me", "user");
	const [setupLoading, setSetupLoading] = useState(false);
	const [removeLoading, setRemoveLoading] = useState(false);

	const handleSetupClicked = async () => {
		setSetupLoading(true);

		let {body} = await initialiseRegistration();
		let {challenge} = body;

		try {
			let response = await navigator.credentials.create({
				publicKey: {
					challenge: Buffer.from(challenge.challenge, "base64"),
					rp: challenge.rp,
					user: {
						id: Buffer.from(challenge.user.id),
						name: challenge.user.name,
						displayName: challenge.user.displayName
					},
					pubKeyCredParams: challenge.pubKeyCredParams,
					authenticatorSelection: challenge.authenticatorSelection
				}
			});

			let responseJson = publicKeyCredentialToJson(response);

			let {error} = await webauthnRegisterResponse(responseJson);

			setSetupLoading(false);

			if (error) {
				alert.error(error.displayMessage);
				return;
			}

			await mutate("/api/me");
			alert.success("Successfully setup passwordless login");
		} catch (err) {
			setSetupLoading(false);

			alert.error(err.message);
		}
	};

	const handleRemove = async () => {
		setRemoveLoading(true);

		let {error} = await remove();

		setRemoveLoading(false);

		if (error) {
			alert.error(error.displayMessage);
			return;
		}

		await mutate("/api/me");
		alert.success("Successfully removed passwordless login");
	};

	if (userLoading) {
		return (
			<>
				<SubHeader
					title="Passwordless Login"
				/>

				<div className="px-8 mt-4 gap-4 lg:px-32">
					<Skeleton height="2rem" width="25%"/>
				</div>
			</>
		);
	}

	return (
		<>
			<SubHeader
				title="Passwordless Login"
			/>

			<div className="px-8 mt-4 gap-4 lg:px-32">
				{!user.webauthnLoginEnabled ?
					<Button colour="green" onClick={handleSetupClicked} loading={setupLoading}>
						Setup
					</Button> : ""
				}

				{user.webauthnLoginEnabled ?
					<>
						<p className="text-xl font font-medium tracking-tight text-white mb-6">
							Passwordless login has been setup.
							<br/>
							<span className="text-orange-400">
								<ExclamationIcon className="h-6 w-6 inline mr-2"/>
								Re-Setting up passwordless login will invalidate the previously used device.
							</span>
						</p>
						<Button colour="green" onClick={handleSetupClicked} loading={setupLoading}>
							Re-Setup
						</Button>

						<Button colour="red" onClick={handleRemove} loading={removeLoading}>
							Remove Existing Device
						</Button>
					</> : ""
				}
			</div>
		</>
	);
};

export default PasswordlessLogin;
