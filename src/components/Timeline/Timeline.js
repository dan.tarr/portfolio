import React from "react";
import {VerticalTimeline} from "react-vertical-timeline-component";

const Timeline = ({children}) => {
	return (
		<VerticalTimeline>
			{children}
		</VerticalTimeline>
	);
};

export default Timeline;
