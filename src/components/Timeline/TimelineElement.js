import React from "react";
import {VerticalTimelineElement} from "react-vertical-timeline-component";
import {calculateTextColorFromHex} from "../../helpers/general";
import Skeleton from "react-loading-skeleton";
import Link from "../Link/Link";

const TimelineElement = ({
	loading = false,
	backgroundColour,
	iconBackgroundColour,
	iconElement,
	dateElement,
	title,
	location,
	body,
	url
}) => {
	const mainTextColour = calculateTextColorFromHex(backgroundColour);
	const iconColour = calculateTextColorFromHex(iconBackgroundColour);

	if (loading) {
		return (
			<VerticalTimelineElement
				className="vertical-timeline-element--work"
				contentStyle={{
					background: backgroundColour
				}}
				contentArrowStyle={{
					borderRight: "7px solid #202020"
				}}
				iconStyle={{
					background: iconBackgroundColour,
					color: "#" + iconColour
				}}
				icon={iconElement}
			>
				<Skeleton
					width="100%"
					height="6rem"
				/>
			</VerticalTimelineElement>
		);
	}

	return (
		<VerticalTimelineElement
			className="vertical-timeline-element--work"
			contentStyle={{
				background: backgroundColour,
				color: "#" + mainTextColour
			}}
			contentArrowStyle={{
				borderRight: `7px solid ${backgroundColour}`
			}}
			date={dateElement}
			iconStyle={{
				background: iconBackgroundColour,
				color: "#" + iconColour
			}}
			icon={iconElement}
		>
			<h3 className="vertical-timeline-element-title font-bold text-lg mb-2">{title}</h3>
			<h4 className="vertical-timeline-element-subtitle">{location}</h4>
			<p className="mt-2">{body}</p>
			{url ?
				<Link
					textClass="underline text-blue-400"
					text={url}
					href={url}
				/> : ""
			}
		</VerticalTimelineElement>
	);
};

export default TimelineElement;
