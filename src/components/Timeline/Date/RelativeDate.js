import React from "react";

const RelativeDate = ({startDateString, endDate, endDateString}) => {
	let relativeDate = endDate.toRelative();

	return (
		<>
			<p>{startDateString} - {endDateString}</p>
			<p>About {relativeDate}</p>
		</>
	);
};

export default RelativeDate;
