import React, {useState} from "react";
import Modal from "./Modal";
import useEndpoint from "../../lib/useEndpoint";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";
import Checkbox from "../FormElements/Checkbox";
import SubmitButton from "../FormElements/SubmitButton";
import Button from "../Button/Button";
import {mutate} from "swr";

const AddSkillTagModal = ({isOpen, onClose, onAdd, existingSkills}) => {
	const {skills, loading: skillsLoading} = useEndpoint("/api/skill", "skills", true, false);

	const [refreshLoading, setRefreshLoading] = useState(false);

	const handleRefreshSkills = async () => {
		setRefreshLoading(true);
		await mutate("/api/skill");
		setRefreshLoading(false);
	};

	const handleAddSkills = (event) => {
		event.preventDefault();
		event.stopPropagation();

		let addedSkills = [];

		for (let element of event.target.elements) {
			let {name, checked, type} = element;

			if (type !== "checkbox")
				continue;

			if (checked)
				addedSkills.push(name);
		}

		onAdd(addedSkills);
	};

	let body;

	if (skillsLoading) {
		body = <>
			<p className="mt-4 mb-2 font-bold mr-40">
				Select Skill
			</p>
			<SkeletonTheme color="#303030" highlightColor="#424242">
				{Array(5).fill().map((val, index) => {
					return <div key={index} className="block mt-3">
						<Skeleton height="1.5rem" width="1.5rem" className="mx-1"/>
						<Skeleton height="1.5rem" width="6rem" className="mx-1"/>
					</div>;
				})}
			</SkeletonTheme>
		</>;
	} else {
		body = <>
			<p className="mt-4 mb-2 font-bold mr-40">
				Select Skills To Add
			</p>

			<form onSubmit={handleAddSkills}>
				{skills.sort((a, b) => {
					if (a.name < b.name)
						return -1;

					if (a.name > b.name)
						return 1;

					return 0;
				}).map(skill => {
					if (existingSkills.includes(skill.id)) {
						return <div key={skill.id} className="mt-3">
							<Checkbox
								disabled={true}
								name={skill.id}
								colour="indigo"
								text={skill.name}
								checked={false}
							/>
						</div>;
					}

					return <div key={skill.id} className="mt-3">
						<Checkbox
							name={skill.id}
							colour="indigo"
							text={skill.name}
						/>
					</div>;
				})}

				<div className="text-center mt-8 space-y-6">
					<Button
						colour="green"
						onClick={handleRefreshSkills}
						loading={refreshLoading}
					>
						Refresh Skills
					</Button>

					<SubmitButton
						text="Add Skill(s)"
					/>
				</div>
			</form>
		</>;
	}

	return (
		<Modal
			isOpen={isOpen}
			onClose={onClose}
			title="Add Skill"
			body={body}
		/>
	);
};

export default AddSkillTagModal;
