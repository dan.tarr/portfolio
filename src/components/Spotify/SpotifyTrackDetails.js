import React from "react";
import SpotifyTrack from "./SpotifyTrack";
import SpotifyArtistsList from "./SpotifyArtistsList";

const SpotifyTrackDetails = ({track, artists}) => {
	return (
		<div className="min-w-0 flex-auto space-y-0.5">
			<SpotifyTrack name={track.name} url={track.url}/>
			<SpotifyArtistsList artists={artists}/>
		</div>
	);
};

export default SpotifyTrackDetails;
