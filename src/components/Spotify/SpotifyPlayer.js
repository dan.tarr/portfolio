import React, {useEffect, useState} from "react";
import useEndpoint from "../../lib/useEndpoint";
import {mutate} from "swr";
import SkeletonImage from "../Image/SkeletonImage";
import SpotifyTrackDetails from "./SpotifyTrackDetails";
import SpotifyTrackProgress from "./SpotifyTrackProgress";

const SpotifyPlayer = () => {
	const {data: spotifyData, loading: spotifyLoading, error} =
		useEndpoint("/api/spotify", "data", false);

	const [progress, setProgress] = useState(null);

	useEffect(() => {
		let timer = setInterval(async () => {
			if (spotifyData) {
				if (spotifyData.playing) {
					if (progress + 1000 < spotifyData.progress.duration)
						setProgress(progress + 1000);
				}
			}
		}, 1000);

		return () => clearInterval(timer);
	}, [progress]);

	useEffect(() => {
		let timer = setInterval(async () => {
			await mutate("/api/spotify");
		}, 5000);

		return () => clearInterval(timer);
	}, []);

	useEffect(() => {
		if (spotifyData) {
			if (spotifyData.playing)
				setProgress(spotifyData.progress.current);
		}
	}, [spotifyData]);

	if (spotifyLoading)
		return null;

	if (error)
		return null;

	if (!spotifyData)
		return null;

	if (!spotifyData.playing)
		return null;

	let songProgressPercentage = (progress /
		spotifyData.progress.duration) * 100;

	return (
		<div className="bg-dark-700 rounded-tl-xl p-4 pb-6 fixed bottom-0 right-0 hidden lg:inline
			lg:p-4 lg:pb-6 space-y-4 lg:w-1/3">

			<h2 className="text-white text-base xl:text-xl font-semibold">
				I&apos;m currently listening to
			</h2>

			<div className="flex items-center space-x-3 mt-1">
				{spotifyData.albumCover ?
					<SkeletonImage
						className="flex-none w-20 h-20 rounded-lg bg-gray-100"
						src={spotifyData.albumCover}
						alt={`${spotifyData.track.name} album cover`}
						skeletonWidth="5rem"
					/> : ""
				}

				<SpotifyTrackDetails track={spotifyData.track} artists={spotifyData.artists}/>
			</div>

			<SpotifyTrackProgress
				progress={progress}
				progressPercentage={songProgressPercentage}
				duration={spotifyData.progress.duration}
			/>
		</div>
	);
};

export default SpotifyPlayer;
