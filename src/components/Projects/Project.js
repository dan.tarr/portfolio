import React from "react";
import useEndpoint from "../../lib/useEndpoint";
import SkillTag from "../Skills/SkillTag";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";
import {DateTime} from "luxon";
import SkeletonImage from "../Image/SkeletonImage";
import Button from "../Button/Button";

const Project = ({id, name, shortDescription, imageUrls = [], createdAt, skillIds = [], openSource,
	onTagClick}) => {
	const {skills, loading: skillsLoading} = useEndpoint("/api/skill", "skills");

	return (
		<div className="p-1">
			<div className="bg-dark-650 p-6 rounded-lg text-white">
				<SkeletonImage
					className="lg:h-60 xl:h-56 md:h-64 sm:h-72 xs:h-72 h-72
					rounded w-full object-cover object-center mb-6"
					src={imageUrls[0]}
					alt={`${name} image`}
				/>

				{skillsLoading ?
					<SkeletonTheme color="#303030" highlightColor="#424242">
						<Skeleton width="3rem" className="mx-1"/>
						<Skeleton width="3rem" className="mx-1"/>
					</SkeletonTheme> :
					<div className="flex flex-wrap justify-center">
						{skillIds.map(skillId => {
							return skills.find(x => x.id === skillId);
						}).sort((a, b) => {
							if (a.name < b.name)
								return -1;

							if (a.name > b.name)
								return 1;

							return 0;
						}).map(skill => {
							return <SkillTag
								key={skill.id}
								skillId={skill.id}
								text={skill.name}
								colour={skill.colour}
								onClick={(clickedId) => onTagClick(skills.find(x => x.id === clickedId))}
							/>;
						})}

						{openSource ?
							<SkillTag
								text="Open Source"
								colour="228B22"
								clickable={false}
							/> : ""
						}
					</div>
				}

				<h2 className="text-lg font-medium title-font my-1">{name}</h2>
				<p className="leading-relaxed text-base">{shortDescription}</p>

				<p className="mt-2 font-bold">
					Created {DateTime.fromMillis(createdAt).toRelative()}
				</p>

				<div className="mt-4">
					<Button colour="indigo" href={`/projects/${id}`}>
						Learn More
					</Button>
				</div>
			</div>
		</div>
	);
};

export default Project;
