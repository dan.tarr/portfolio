import React from "react";
import SmallHeader from "../Header/SmallHeader";
import Skeleton from "react-loading-skeleton";
import TextInput from "../FormElements/TextInput";
import ProjectProperty from "./ProjectProperty";
import TextAreaInput from "../FormElements/TextAreaInput";
import Checkbox from "../FormElements/Checkbox";
import ReadOnlyLinkInput from "../FormElements/ReadOnlyLinkInput";
import BulletList from "../BulletList/BulletList";
import BulletListItem from "../BulletList/BulletListItem";
import Contributor from "../Admin/Contributor/Contributor";
import {DateTime} from "luxon";
import SkillTag from "../Skills/SkillTag";
import useEndpoint from "../../lib/useEndpoint";
import EnlargeableImage from "../Image/EnlargeableImage";

const ProjectPage = ({loading, project}) => {
	const {skills, loading: skillsLoading} = useEndpoint("/api/skill", "skills");

	if (loading || skillsLoading) {
		return <>
			<SmallHeader
				title={<Skeleton/>}
				subtitle={<Skeleton/>}
			/>

			<div className="grid grid-cols-1 text-center gap-4 mx-auto space-x-8
				mb-12 lg:w-4/5 lg:grid-cols-2">
				<div className="space-y-4 mx-8 lg:mx-0">
					<ProjectProperty name="Project Name">
						<Skeleton height="2.5rem"/>
					</ProjectProperty>

					<ProjectProperty name="Short Description">
						<Skeleton height="2.5rem"/>
					</ProjectProperty>

					<ProjectProperty name="Long Description">
						<Skeleton height="2.5rem"/>
					</ProjectProperty>

					<ProjectProperty name="Skills">
						<Skeleton height="2.5rem"/>
					</ProjectProperty>

					<ProjectProperty name="Open Source">
						<Skeleton height="2rem"/>
					</ProjectProperty>

					<ProjectProperty name="Repository">
						<Skeleton height="2.5rem"/>
					</ProjectProperty>

					<ProjectProperty name="Contributors">
						<div className="mx-10 my-2 text-left">
							<BulletList>
								{Array(2).fill().map((value, index) => {
									return <BulletListItem key={index}>
										<Skeleton height="2rem"/>
									</BulletListItem>;
								})}
							</BulletList>
						</div>
					</ProjectProperty>

					<ProjectProperty name="Date Added">
						<Skeleton height="2.5rem"/>
					</ProjectProperty>
				</div>

				<Skeleton height="20rem"/>
			</div>
		</>;
	}

	const dateAdded = DateTime.fromMillis(project.dateAdded);
	const formattedDateAdded = `${dateAdded
		.toLocaleString(DateTime.DATE_FULL)} (${dateAdded.toRelative()})`;

	return (
		<>
			<SmallHeader
				title={project.name}
				subtitle={project.shortDescription}
			/>

			<div className="grid grid-cols-1 text-center gap-4 mx-auto space-x-8
				mb-12 lg:w-4/5 lg:grid-cols-2">
				<div className="space-y-4 mx-8 lg:mx-0">
					<ProjectProperty name="Project Name">
						<TextInput
							readOnly={true}
							defaultValue={project.name}
							position="alone"
						/>
					</ProjectProperty>

					<ProjectProperty name="Short Description">
						<TextInput
							readOnly={true}
							defaultValue={project.shortDescription}
							position="alone"
						/>
					</ProjectProperty>

					<ProjectProperty name="Long Description">
						<TextAreaInput
							readOnly={true}
							defaultValue={project.longDescription}
							position="alone"
							rows={15}
						/>
					</ProjectProperty>

					<ProjectProperty name="Skills">
						<div className={`form-input appearance-none rounded-none block w-full px-4
							py-3 bg-dark-650 border border-dark-500 placeholder-gray-200 text-white
							sm:text-sm rounded-t-md rounded-b-md`}>
							{project.skills.map(projectSkill => {
								let skill = skills.find(x => x.id === projectSkill.skillId);

								return <SkillTag
									key={skill.id}
									skillId={skill.id}
									text={skill.name}
									colour={skill.colour}
									clickable={false}
								/>;
							})}

							{project.skills.length === 0 ?
								<p className="text-red-400 font-bold text-lg">No Selected Skills</p> :
								""
							}
						</div>
					</ProjectProperty>

					<ProjectProperty name="Open Source">
						<Checkbox
							colour="indigo"
							checked={project.openSource}
							readOnly={true}
							disabled={true}
							size={8}
						/>
					</ProjectProperty>

					<ProjectProperty name="Repository">
						<ReadOnlyLinkInput href={project.repository}/>
					</ProjectProperty>

					<ProjectProperty name="Contributors">
						<div className="mx-10 my-2 text-left">
							<BulletList>
								{project.contributors.map((contributor, index) => {
									return <BulletListItem key={index}>
										<Contributor
											name={contributor.name}
											link={contributor.link}
										/>
									</BulletListItem>;
								})}
							</BulletList>
						</div>
					</ProjectProperty>

					<ProjectProperty name="Date Added">
						<TextInput
							readOnly={true}
							defaultValue={formattedDateAdded}
							position="alone"
						/>
					</ProjectProperty>
				</div>

				<div>
					{project.imageUrls.map(imageUrl => {
						return <EnlargeableImage
							key={imageUrl}
							src={imageUrl}
							alt={`${project.name} image`}
						/>;
					})}
				</div>

			</div>
		</>
	);
};

export default ProjectPage;
