import React from "react";
import TimelineElement from "../Timeline/TimelineElement";
import Timeline from "../Timeline/Timeline";
import {BookOpenIcon} from "@heroicons/react/outline";
import useEndpoint from "../../lib/useEndpoint";
import RelativeDate from "../Timeline/Date/RelativeDate";
import {DateTime} from "luxon";

const EducationList = () => {
	const {education, loading: educationLoading} = useEndpoint("/api/education", "education");

	if (educationLoading) {
		return (
			<div className="text-center px-8 gap-4 lg:grid-cols-3 lg:px-32">
				<Timeline>
					{Array(2).fill().map((val, i) => {
						return <TimelineElement
							key={i}
							loading={true}
							iconElement={<BookOpenIcon/>}
							backgroundColour="#202020"
							iconBackgroundColour="#202020"
						/>;
					})}
				</Timeline>
			</div>
		);
	}

	return (
		<div className="text-center px-8 gap-4 lg:grid-cols-3 lg:px-32">
			<Timeline>
				{education.map(educationObject => {
					let dateElement = <RelativeDate
						startDate={DateTime.fromObject({year: educationObject.startDate})}
						startDateString={educationObject.startDate}
						endDate={educationObject.endDate ?
							DateTime.fromObject({year: educationObject.endDate}) : null
						}
						endDateString={educationObject.endDate}
					/>;

					return <TimelineElement
						key={educationObject.id}
						backgroundColour="#252525"
						iconBackgroundColour="#000000"
						iconElement={<BookOpenIcon/>}
						startDateString={educationObject.startDate}
						endDate={educationObject.endDate}
						dateElement={dateElement}
						title={educationObject.level}
						location={educationObject.location}
						body={`${educationObject.grade} in ${educationObject.subject}`}
					/>;
				})}
			</Timeline>
		</div>
	);
};

export default EducationList;
