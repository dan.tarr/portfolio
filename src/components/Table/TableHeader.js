import React from "react";

const TableHeader = ({text}) => {
	return (
		<th
			scope="col"
			className="px-6 py-3 text-left text-xs font-medium text-white
			uppercase tracking-wider bg-dark-700"
		>
			{text}
		</th>
	);
};

export default TableHeader;
