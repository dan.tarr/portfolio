import React from "react";

const TableRow = ({children}) => {
	return (
		<tr className="bg-dark-650">
			{children}
		</tr>
	);
};

export default TableRow;
