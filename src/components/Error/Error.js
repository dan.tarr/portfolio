import React from "react";
import {XIcon} from "@heroicons/react/solid";

const Error = ({message}) => {
	return (
		<div className="mt-2">
			<div className="inline-flex items-center text-red-500">
				<XIcon className="h-6 mr-1 inline-block"/>
				<p className="inline-block">{message}</p>
			</div>
		</div>
	);
};

export default Error;
