import React from "react";
import {calculateTextColorFromHex} from "../../helpers/general";
import {XIcon} from "@heroicons/react/solid";

const SkillTag = ({
	skillId,
	text,
	colour,
	showDeleteIcon = false,
	onClick,
	onDelete,
	clickable = true
}) => {
	let textColour = calculateTextColorFromHex(colour);

	return (
		<>
			<span className={`m-1 rounded-full px-2 ${clickable ? "cursor-pointer" : ""}
				font-bold text-sm leading-loose inline-flex items-center`} style={{
				backgroundColor: "#" + colour,
				color: "#" + textColour
			}} onClick={() => onClick ? onClick(skillId) : null}>
				{text}

				{showDeleteIcon ?
					<XIcon className="h-4 w-5 inline-block ml-0.5" onClick={onDelete}/> : ""
				}
			</span>
		</>
	);
};

export default SkillTag;
