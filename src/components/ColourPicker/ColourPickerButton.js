import React, {useState} from "react";
import CustomColorPicker from "./CustomColourPicker";
import { calculateTextColorFromHex } from "../../helpers/general";

const ColourPickerButton = ({ setColour, colour, disabled, className }) => {
	const [displayColorPicker, setDisplayColorPicker] = useState(false);

	const handleClick = () => {
		setDisplayColorPicker(!displayColorPicker);
	};

	const handleClose = () => {
		setDisplayColorPicker(false);
	};

	const handleChangeComplete = (changedColor) => {
		setColour(changedColor.hex.substr(1));
	};

	const popover = "absolute z-10";
	const cover = "fixed p-0 inset-0";

	let classes = `disabled-button-wrapper position-relative w-100 ${className ? className : ""}`;

	return (
		<>
			<span id="colourButton" className={classes}>
				<button
					className="relative normal-case block w-full bg-red-400 rounded-md
						leading-6 align-middle border border-transparent border-solid my-4 h-12"
					onClick={handleClick}
					data-colour={colour}
					name="colour"
					type="button"
					disabled={disabled}
					style={{
						backgroundColor: `#${colour}`,
						borderColor: `#${colour}40`,
						color: `#${calculateTextColorFromHex(colour)}`
					}}
				>
					Choose Colour
				</button>
				{displayColorPicker ?
					<div className={popover}>
						<div className={cover} onClick={handleClose}/>
						<CustomColorPicker onChangeComplete={handleChangeComplete}
							color={colour} disableAlpha={true} presetColors={[]} />
					</div> : null
				}
			</span>
		</>
	);
};

export default ColourPickerButton;
