import React from "react";
import Link from "next/link";

const MobileNavBarItem = ({text, href, active}) => {
	if (active) {
		return (
			<Link href={href} passHref>
				<a href={href} className="bg-dark-800 text-white block px-3 py-2
					rounded-md text-base font-medium">
					{text}
				</a>
			</Link>
		);
	}

	return (
		<Link href={href} passHref>
			<a href={href} className="text-gray-300 hover:bg-dark-750 hover:text-white
				block px-3 py-2 rounded-md text-base font-medium">
				{text}
			</a>
		</Link>
	);
};

export default MobileNavBarItem;
