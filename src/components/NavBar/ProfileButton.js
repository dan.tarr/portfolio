import React from "react";
import useEndpoint from "../../lib/useEndpoint";
import NavBarItem from "./NavBarItem";
import {useRouter} from "next/router";

const ProfileButton = ({onClick}) => {
	const router = useRouter();
	const {user, loading: userLoading, error} = useEndpoint("/api/me", "user", false, false);

	let {asPath} = router;
	let currentPath = asPath.split("?")[0];

	if (userLoading)
		return null;

	if (error) {
		return <NavBarItem
			text="Login"
			href="/login"
			active={currentPath === "/login"}
			poggers={"igfjdgiodfjgifgjo"}
		/>;
	}

	return (
		<div>
			<button type="button" className="bg-gray-800 flex text-sm rounded-full
				focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800
				focus:ring-white" onClick={onClick}
			aria-expanded="false" aria-haspopup="true">
				<img className="h-8 w-8 rounded-full" src={user.avatarUrl} alt=""/>
			</button>
		</div>
	);
};

export default ProfileButton;
