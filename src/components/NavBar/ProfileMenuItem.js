import React from "react";
import Link from "next/link";

const ProfileNavBarItem = ({text, href}) => {
	return (
		<Link href={href} passHref>
			<a href={href} className="block px-4 py-2 text-sm text-white hover:bg-dark-700">
				{text}
			</a>
		</Link>
	);
};

export default ProfileNavBarItem;
