import React from "react";
import {pages} from "../../lib/constants";
import MobileNavBarItem from "./MobileNavBarItem";
import {useRouter} from "next/router";

const MobileNavMenu = () => {
	const router = useRouter();

	let {asPath} = router;
	let currentPath = asPath.split("?")[0];

	return (
		<div className="sm:hidden">
			<div className="px-2 pt-2 pb-3 space-y-1">
				{pages.map(page => {
					return <MobileNavBarItem
						key={page.url}
						text={page.name}
						href={page.url}
						active={currentPath === page.url}
					/>;
				})}
			</div>
		</div>
	);
};

export default MobileNavMenu;
