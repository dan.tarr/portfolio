import React from "react";
import ProfileMenuItem from "./ProfileMenuItem";
import {profilePages} from "../../lib/constants";

const ProfileMenu = ({visible, onBlur}) => {
	if (!visible)
		return null;

	return (
		<div className="bg-dark-600 origin-top-right absolute right-0 mt-2 w-48 rounded-md
			shadow-lg z-10 py-1 ring-1 ring-black ring-opacity-5 focus:outline-none" onBlur={onBlur}
		role="menu">
			{profilePages.map(page => {
				return <ProfileMenuItem key={page.url} href={page.url} text={page.name}/>;
			})}
		</div>
	);
};

export default ProfileMenu;
