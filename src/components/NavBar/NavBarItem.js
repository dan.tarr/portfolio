import React from "react";
import Link from "next/link";

const NavBarItem = ({text, href, active}) => {
	if (active) {
		return (
			<Link href={href} passHref>
				<a href={href} className="bg-dark-800 text-white px-3
					py-2 rounded-md text-sm font-medium">
					{text}
				</a>
			</Link>
		);
	}

	return (
		<Link href={href} passHref>
			<a href={href} className="text-gray-300 hover:bg-dark-750
				hover:text-white px-3 py-2 rounded-md text-sm font-medium">
				{text}
			</a>
		</Link>
	);
};

export default NavBarItem;
