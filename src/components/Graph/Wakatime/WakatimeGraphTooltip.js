import React from "react";
import {DateTime, Duration} from "luxon";
import {pluralise} from "../../../helpers/general";

const WakatimeGraphTooltip = ({active, payload, label}) => {
	if (active && payload && payload.length) {
		let [currentPayload] = payload;
		let {value} = currentPayload;

		let parsedLabel = DateTime.fromFormat(label, "yyyy-MM-dd")
			.toFormat("DD");
		let parsedValue = Duration.fromObject({
			hours: value
		}).shiftTo("hours", "minutes", "seconds")
			.toObject();

		parsedValue.seconds = Math.floor(parsedValue.seconds);

		let hours = `${parsedValue.hours} ${pluralise(parsedValue.hours, "hour")}`;
		let minutes = `${parsedValue.minutes} ${pluralise(parsedValue.minutes, "minute")}`;
		let seconds = `${parsedValue.seconds} ${pluralise(parsedValue.seconds, "second")}`;
		let valueString = `${hours}, ${minutes} and ${seconds}`;

		return (
			<div className="bg-dark-650 p-2 rounded-xl shadow-2xl border border-dark-100">
				<p>{parsedLabel}</p>
				<p>{valueString}</p>
			</div>
		);
	}

	return null;
};

export default WakatimeGraphTooltip;
