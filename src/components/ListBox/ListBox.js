import React, {Fragment, useState} from "react";
import {Listbox, Transition} from "@headlessui/react";
import {CheckIcon, SelectorIcon} from "@heroicons/react/solid";

const {
	Button,
	Option,
	Options
} = Listbox;

const ListBox = ({options, defaultValue, onChange, prefix, disabled}) => {
	const [selected, setSelected] = useState(defaultValue);

	const handleChange = (newValue) => {
		setSelected(newValue);
		onChange(newValue);
	};

	return (
		<Listbox value={selected} onChange={handleChange} disabled={disabled}>
			<div className="mt-1 relative">
				<Button className="relative w-11/12 bg-dark-700 border border-dark-500 rounded-md
					shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1
					focus:ring-dark-400 focus:border-dark-400 sm:text-sm disabled:opacity-50">

					<span className="flex items-center">
						<span className="ml-1 block truncate">
							{prefix ? prefix + ": " : ""} {selected.text}
						</span>
					</span>

					<span className="ml-3 absolute inset-y-0 right-0 flex
						items-center pr-2 pointer-events-none">
						<SelectorIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
					</span>
				</Button>

				<Transition
					as={Fragment} leave="transition ease-in duration-100"
					leaveFrom="opacity-100" leaveTo="opacity-0">
					<Options className="absolute z-10 mt-1 w-11/12 bg-dark-700 shadow-lg ml-2
						max-h-56 rounded-md py-1 text-base white ring-1 ring-black ring-opacity-5
						overflow-auto focus:outline-none sm:text-sm">
						{options.map((sortBy) => (
							<Option
								key={sortBy.value}
								className={({active}) =>
									`${active ? "bg-dark-800" : ""} cursor-default
										select-none relative py-2 pl-3 pr-9 text-white`
								}
								value={sortBy}
							>
								{({selected: isSelected}) => (
									<>
										<div className="flex items-center">
											<span className={`${isSelected ? "font-semibold" :"font-normal"}
												ml-1 block truncate`}>
												{sortBy.text}
											</span>
										</div>

										{isSelected ? (
											<span
												className={`text-white absolute
													inset-y-0 right-0 flex items-center pr-4`}
											>
												<CheckIcon className="h-5 w-5" aria-hidden="true"/>
											</span>
										) : null}
									</>
								)}
							</Option>
						))}
					</Options>
				</Transition>
			</div>
		</Listbox>
	);
};

export default ListBox;
