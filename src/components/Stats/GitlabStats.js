import React from "react";
import useEndpoint from "../../lib/useEndpoint";
import SubHeader from "../Header/SubHeader";
import Skeleton from "react-loading-skeleton";
import BulletList from "../BulletList/BulletList";
import BulletListItem from "../BulletList/BulletListItem";
import {DateTime} from "luxon";
import HighlightedStatistic from "./HighlightedStatistic";

const GitlabStats = () => {
	const {data: gitlabData, loading: gitlabLoading} = useEndpoint("/api/gitlab", "data",
		true, false);

	if (gitlabLoading) {
		return (
			<>
				<SubHeader
					title="GitLab Stats"
				/>

				<div className="px-8 gap-4 mb-6 lg:px-32">
					<BulletList className="mt-4">
						{Array(4).fill().map((val, index) => {
							return <BulletListItem key={index}>
								<Skeleton width="60%" height="1.5rem"/>
							</BulletListItem>;
						})}
					</BulletList>
				</div>
			</>
		);
	}

	let bestDay = DateTime.fromFormat(gitlabData.highestDay.date, "yyyy-MM-dd");
	let daysSinceBestDay = Math.floor(Math.abs(bestDay.diffNow(["days"])
		.toObject()
		.days));

	return (
		<>
			<SubHeader
				title="GitLab Stats"
				subtitle="GitLab is my prefered Git platform, so most of my activity will occur here"
			/>

			<div className="px-8 gap-4 mb-8 lg:px-32">
				<BulletList className="mt-4">
					<BulletListItem>
						I have made
						<HighlightedStatistic padText={true}>
							{gitlabData.contributionStreak.contributions.toLocaleString()}
						</HighlightedStatistic>
						contributions over the last
						<HighlightedStatistic padText={true}>
							{gitlabData.contributionStreak.days}
						</HighlightedStatistic>
						consecutive days.
					</BulletListItem>

					<BulletListItem>
						I have made
						<HighlightedStatistic padText={true}>
							{gitlabData.totalContributions.toLocaleString()}
						</HighlightedStatistic>
						contributions in the last year.
					</BulletListItem>

					<BulletListItem>
						My best day was
						<HighlightedStatistic padText={true}>
							{daysSinceBestDay} days ago
						</HighlightedStatistic>
						where I
						made
						<HighlightedStatistic padText={true}>
							{gitlabData.highestDay.count}
						</HighlightedStatistic>
						contributions
					</BulletListItem>

					<BulletListItem>
						My last contribution was
						<HighlightedStatistic padText={true}>
							{DateTime.fromMillis(gitlabData.lastContributionTimestamp).toRelative()}
						</HighlightedStatistic>
					</BulletListItem>
				</BulletList>
			</div>
		</>
	);
};

export default GitlabStats;
