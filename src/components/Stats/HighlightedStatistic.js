import React from "react";

const HighlightedStatistic = ({children, padText}) => {
	return (
		<span className="text-indigo-400">
			{padText ? " " : ""}
			{children}
			{padText ? " " : ""}
		</span>
	);
};

export default HighlightedStatistic;
