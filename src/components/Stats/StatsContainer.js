import React from "react";
import GitlabStats from "./GitlabStats";
// import WakatimeStats from "./WakatimeStats";
import GithubStats from "./GithubStats";

const StatsContainer = () => {
	return (
		<>
			{/* <WakatimeStats/> */}
			<GitlabStats/>
			<GithubStats/>
		</>
	);
};

export default StatsContainer;
