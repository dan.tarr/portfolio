import React, {useState} from "react";
import NewProjectForm from "../../Form/NewProjectForm";
import Project from "../../Projects/Project";

const NewProject = () => {
	const [newProject, setNewProject] = useState({
		name: "",
		shortDescription: "",
		longDescription: "",
		openSource: false,
		repository: "",
		imageUrl: "",
		contributors: [],
		skills: []
	});

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<NewProjectForm
				onChange={(newValue) => setNewProject({
					...newProject,
					[newValue.name]: newValue.value
				})}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<Project
					name={newProject.name}
					shortDescription={newProject.shortDescription}
					imageUrls={newProject.imageUrls}
					createdAt={new Date().getTime() - 1000}
					skillIds={newProject.skills}
				/>
			</div>
		</div>
	);
};

export default NewProject;
