import React, {useState} from "react";
import BulletList from "../../BulletList/BulletList";
import BulletListItem from "../../BulletList/BulletListItem";
import Contributor from "../Contributor/Contributor";
import Button from "../../Button/Button";
import AddContributorModal from "../../Modal/AddContributorModal";

const EditableContributorsList = ({onChange, defaultContributors = []}) => {
	const [contributors, setContributors] = useState(defaultContributors);
	const [addContributorOpen, setAddContributorOpen] = useState(false);

	const handleAddContributor = (contributor) => {
		let newContributors = [...contributors, contributor];
		setContributors(newContributors);
		setAddContributorOpen(false);
		onChange(newContributors);
	};

	return (
		<div className="mt-4">
			<h1 className="font-bold text-left text-xl">
				Contributors
			</h1>
			<div className="mx-10 my-4 text-left">
				<BulletList>
					{contributors.map((contributor, index) => {
						return <BulletListItem key={index}>
							<Contributor
								name={contributor.name}
								link={contributor.link}
								showDeleteIcon={true}
								onDelete={() => {
									let newContributors = contributors.filter(x => !(x.name === contributor.name &&
										x.link === contributor.link));
									setContributors(newContributors);
									onChange(newContributors);
								}}
							/>
						</BulletListItem>;
					})}
				</BulletList>
			</div>

			<div className="mt-6 mb-6">
				<Button colour="green" onClick={() => setAddContributorOpen(true)}>
					Add Contributor
				</Button>
			</div>

			<AddContributorModal
				isOpen={addContributorOpen}
				onClose={() => setAddContributorOpen(false)}
				onAdd={handleAddContributor}
			/>
		</div>
	);
};

export default EditableContributorsList;
