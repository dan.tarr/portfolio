import React from "react";
import TableHeader from "../../Table/TableHeader";
import TableRow from "../../Table/TableRow";
import TableCell from "../../Table/TableCell";
import Link from "next/link";
import Button from "../../Button/Button";
import {CheckIcon, XIcon} from "@heroicons/react/solid";

const AdminProjectTable = ({projects, onProjectDeleteClick}) => {
	return (
		<div className="flex flex-col mx-4 mb-6 mt-6 lg:mx-32">
			<div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div className="shadow overflow-hidden border-b border-dark-200 rounded-lg">
						<table className="min-w-full divide-y divide-dark-200">
							<thead>
								<tr>
									<TableHeader
										text="Name"
									/>
									<TableHeader
										text="Short Description"
									/>
									<TableHeader
										text="Respository"
									/>
									<TableHeader
										text="Open Source"
									/>
									<TableHeader
										text="Actions"
									/>
								</tr>
							</thead>
							<tbody className="divide-y divide-dark-200">
								{projects.map((project) => (
									<TableRow key={project.id}>
										<TableCell>
											<div className="flex-shrink-0 h-10 w-10">
												<img className="object-cover object-center"
													src={project.imageUrls[0]} alt=""/>
											</div>
											<div className="ml-4">
												<div className="text-sm font-medium">
													{project.name}
												</div>
											</div>
										</TableCell>

										<TableCell>
											{project.shortDescription}
										</TableCell>

										<TableCell>
											<Link href={project.repository}>
												{project.repository}
											</Link>
										</TableCell>

										<TableCell>
											{project.openSource ?
												<CheckIcon className="h-8 w-8 text-green-500"/> :
												<XIcon className="h-8 w-8 text-red-500"/>
											}
										</TableCell>

										<TableCell>
											<Button
												colour="yellow"
												textColour="gray-800"
												href={`/admin/projects/edit/${project.id}`}
											>
												Edit
											</Button>
											<Button
												colour="red"
												textColour="white"
												onClick={() => onProjectDeleteClick(project.id)}
											>
												Delete
											</Button>
										</TableCell>
									</TableRow>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

export default AdminProjectTable;
