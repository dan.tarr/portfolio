import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import Project from "../../Projects/Project";
import EditProjectForm from "../../Form/EditProjectForm";

const EditProject = () => {
	const router = useRouter();
	let {id} = router.query;

	if (!id)
		return null;

	const [project, setProject] = useState(null);
	const {project: currentProject, loading: projectsLoading} = useEndpoint(`/api/project/${id}`,
		"project", true, false);

	useEffect(() => {
		if (currentProject)
			setProject(currentProject);
	}, [currentProject]);

	if (projectsLoading || !project) {
		return (
			<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

				<div className="flex justify-center py-12 px-0 lg:px-8">
					<div className="max-w-md w-full space-y-8 mt-8">
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="3rem" className="my-2"/>
						<Skeleton height="2.375rem" className="my-2"/>
					</div>
				</div>

				<div>
					<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
						Preview
					</h1>

					<h1 className="mb-4 ml-4">
						<Skeleton width="30%"/>
					</h1>
					<Skeleton height="20rem"/>
				</div>
			</div>
		);
	}

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<EditProjectForm
				onChange={(newValue) => setProject({
					...project,
					[newValue.name]: newValue.value
				})}
				baseProject={project}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<Project
					name={project.name}
					shortDescription={project.shortDescription}
					imageUrls={project.imageUrls}
					createdAt={new Date().getTime() - 1000}
					skillIds={project.skills.map(x => x.skillId)}
				/>
			</div>
		</div>
	);
};

export default EditProject;
