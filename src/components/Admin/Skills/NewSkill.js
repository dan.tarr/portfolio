import React, {useState} from "react";
import Skill from "../../Skills/Skill";
import NewSkillForm from "../../Form/NewSkillForm";

const NewSkill = () => {
	const [newSkill, setNewSkill] = useState({
		id: null,
		type: "",
		name: "",
		link: "",
		imageUrl: "",
		colour: "ffffff",
		projectCount: 0
	});

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<NewSkillForm
				onChange={(newValue) => setNewSkill({
					...newSkill,
					[newValue.name]: newValue.value
				})}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<h1 className="text-2xl text-left font font-extrabold tracking-tight text-white mb-4 ml-4">
					{newSkill.type}
				</h1>
				<Skill {...newSkill}/>
			</div>
		</div>
	);
};

export default NewSkill;
