import React from "react";
import {library} from "@fortawesome/fontawesome-svg-core";
import {fas} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "next/link";
library.add(fas);

const AdminPageCard = ({title, body, icon, url}) => {
	return (
		<Link className="p-1" href={url}>
			<div className="bg-dark-650 p-6 rounded-lg text-white cursor-pointer">
				<FontAwesomeIcon
					icon={icon}
					className="mx-auto mb-2 w-12 h-12"
					color="#ffffff"
				/>

				<h2 className="text-lg font-medium title-font my-1">{title}</h2>

				<p>{body}</p>
			</div>
		</Link>
	);
};

export default AdminPageCard;
