import React, {useState} from "react";
import Contact from "../../Contact/Contact";
import NewContactForm from "../../Form/NewContactForm";

const NewContact = () => {
	const [newContact, setNewContact] = useState({
		title: "",
		text: "",
		link: "",
		baseSiteLink: "",
		icon: "",
		iconColour: ""
	});

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<NewContactForm
				onChange={(newValue) => setNewContact({
					...newContact,
					[newValue.name]: newValue.value
				})}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<Contact {...newContact}/>
			</div>
		</div>
	);
};

export default NewContact;
