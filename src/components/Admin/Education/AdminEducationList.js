import React, {useState} from "react";
import {useAlert} from "react-alert";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import {mutate} from "swr";
import Button from "../../Button/Button";
import AdminEducationTable from "./AdminEducationTable";
import DeleteEducationModal from "../../Modal/DeleteEducationModal";
import deleteEducation from "../../../helpers/apiMethods/education/deleteEducation";

const AdminEducationList = () => {
	const alert = useAlert();
	const {education, loading: educationLoading} = useEndpoint("/api/education", "education",
		true, false);

	const [isOpen, setIsOpen] = useState(false);
	const [selectedEducationToDelete, setSelectedEducationToDelete] = useState(false);
	const [educationDeleteLoading, setEducationDeleteLoading] = useState(false);

	if (educationLoading) {
		return (
			<div className="text-center px-8 gap-4 lg:px-32">
				{Array(12).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="3rem"
					/>;
				})}
			</div>
		);
	}

	const handleEducationDeleteClicked = (educationId) => {
		setIsOpen(true);
		setSelectedEducationToDelete(educationId);
	};

	const handleEducationDeleteConfirm = async () => {
		setEducationDeleteLoading(true);

		let {error} = await deleteEducation(selectedEducationToDelete);

		if (error) {
			setEducationDeleteLoading(false);
			alert.error(`Failed to delete education: ${error.displayMessage}`);
			return;
		}

		await mutate("/api/education");
		setEducationDeleteLoading(false);
		setIsOpen(false);
		alert.success("Successfully deleted education");
	};

	return (
		<>
			<div className="mb-1 ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4 lg:px-6 lg:mb-8">
				<Button colour="green" href="/admin/education/new">
					Create New
				</Button>
			</div>

			<AdminEducationTable
				education={education}
				onEducationDeleteClick={handleEducationDeleteClicked}
			/>

			<DeleteEducationModal
				isOpen={isOpen}
				onClose={() => setIsOpen(false)}
				onDelete={handleEducationDeleteConfirm}
				deleteLoading={educationDeleteLoading}
			/>
		</>
	);
};

export default AdminEducationList;
