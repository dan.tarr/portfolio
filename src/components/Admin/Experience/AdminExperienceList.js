import React, {useState} from "react";
import {useAlert} from "react-alert";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import {mutate} from "swr";
import Button from "../../Button/Button";
import AdminExperienceTable from "./AdminExperienceTable";
import DeleteExperienceModal from "../../Modal/DeleteExperienceModal";
import deleteExperience from "../../../helpers/apiMethods/experience/deleteExperience";

const AdminExperienceList = () => {
	const alert = useAlert();
	const {experience, loading: experienceLoading} = useEndpoint("/api/experience", "experience",
		true, false);

	const [isOpen, setIsOpen] = useState(false);
	const [selectedExperienceToDelete, setSelectedExperienceToDelete] = useState(false);
	const [experienceDeleteLoading, setExperienceDeleteLoading] = useState(false);

	if (experienceLoading) {
		return (
			<div className="text-center px-8 gap-4 lg:px-32">
				{Array(12).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="3rem"
					/>;
				})}
			</div>
		);
	}

	const handleExperienceDeleteClicked = (experienceId) => {
		setIsOpen(true);
		setSelectedExperienceToDelete(experienceId);
	};

	const handleExperienceDeleteConfirm = async () => {
		setExperienceDeleteLoading(true);

		let {error} = await deleteExperience(selectedExperienceToDelete);

		if (error) {
			setExperienceDeleteLoading(false);
			alert.error(`Failed to delete experience: ${error.displayMessage}`);
			return;
		}

		await mutate("/api/experience");
		setExperienceDeleteLoading(false);
		setIsOpen(false);
		alert.success("Successfully deleted experience");
	};

	return (
		<>
			<div className="mb-1 ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4 lg:px-6 lg:mb-8">
				<Button colour="green" href="/admin/experience/new">
					Create New
				</Button>
			</div>

			<AdminExperienceTable
				experience={experience}
				onExperienceDeleteClick={handleExperienceDeleteClicked}
			/>

			<DeleteExperienceModal
				isOpen={isOpen}
				onClose={() => setIsOpen(false)}
				onDelete={handleExperienceDeleteConfirm}
				deleteLoading={experienceDeleteLoading}
			/>
		</>
	);
};

export default AdminExperienceList;
