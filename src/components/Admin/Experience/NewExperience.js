import React, {useState} from "react";
import {DateTime} from "luxon";
import TimelineElement from "../../Timeline/TimelineElement";
import NewExperienceForm from "../../Form/NewExperienceForm";
import DurationDate from "../../Timeline/Date/DurationDate";

const NewExperience = () => {
	const [newExperience, setNewExperience] = useState({
		title: "",
		description: "",
		company: "",
		url: "",
		iconUrl: "",
		endDate: 0,
		startDate: 0
	});

	let iconBackgroundColour = "#252525";

	if (newExperience.iconUrl)
		iconBackgroundColour = "#ffffff00";

	let startDate = DateTime.fromMillis(newExperience.startDate);
	let endDate = newExperience.endDate ?
		DateTime.fromMillis(newExperience.endDate) :
		null;
	let dateElement = <DurationDate
		startDate={newExperience.startDate}
		startDateString={startDate.toFormat("MMMM yyyy")}
		endDate={newExperience.endDate}
		endDateString={endDate ? endDate.toFormat("MMMM yyyy") : null}
	/>;

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<NewExperienceForm
				onChange={(newValue) => setNewExperience({
					...newExperience,
					[newValue.name]: newValue.value
				})}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<TimelineElement
					backgroundColour="#252525"
					iconBackgroundColour={iconBackgroundColour}
					iconElement={<img src={newExperience.iconUrl} alt="Icon"/>}
					startDate={newExperience.startDate}
					startDateString={startDate.toFormat("MMMM yyyy")}
					endDate={endDate}
					dateElement={dateElement}
					title={newExperience.title}
					location={newExperience.company}
					body={newExperience.description}
					url={newExperience.url}
				/>
			</div>
		</div>
	);
};

export default NewExperience;
