import React from "react";
import Link from "next/link";
import {XIcon} from "@heroicons/react/solid";

const ImageURL = ({url, onDelete}) => {
	return (
		<>
			<Link href={url} passHref>
				<a className="text-blue-400" target="_blank">
					{url}
				</a>
			</Link>

			<XIcon className="h-6 w-6 inline-block ml-2 text-red-500 cursor-pointer"
				onClick={onDelete}/>
		</>
	);
};

export default ImageURL;
