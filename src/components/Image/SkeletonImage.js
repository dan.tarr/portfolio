import React, {useState} from "react";
import Skeleton from "react-loading-skeleton";

const SkeletonImage = ({className, src, alt, skeletonWidth, onClick}) => {
	const [imageLoading, setImageLoading] = useState(true);

	return (
		<>
			{imageLoading ?
				<Skeleton className={className} width={skeletonWidth}/> : ""
			}
			<img
				className={className}
				src={src}
				hidden={imageLoading}
				alt={alt}
				onLoad={() => setImageLoading(false)}
				onClick={onClick}
			/>
		</>
	);
};

export default SkeletonImage;
