import React, {useState} from "react";
import SkeletonImage from "./SkeletonImage";
import FullWidthModal from "../Modal/FullWidthModal";

const EnlargeableImage = ({src, alt}) => {
	const [isOpen, setIsOpen] = useState(false);

	let imageElement = <SkeletonImage
		className="rounded w-auto object-cover object-center mt-6 mx-auto"
		src={src}
		alt={alt}
		onClick={() => setIsOpen(true)}
	/>;

	return (
		<>
			<SkeletonImage
				className="h-80 border border-dark-400 cursor-zoom-in
				rounded w-auto object-cover object-center mb-6 mx-auto"
				src={src}
				alt={alt}
				onClick={() => setIsOpen(true)}
			/>

			<FullWidthModal
				isOpen={isOpen}
				onClose={() => setIsOpen(false)}
				title="View Image"
				body={imageElement}
			/>
		</>
	);
};

export default EnlargeableImage;
