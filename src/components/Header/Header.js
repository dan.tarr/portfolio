import React from "react";

const Header = ({title, subtitle, width = "3xl"}) => {
	return (
		<div className="relative overflow-hidden">
			<div className="pt-16 pb-80 sm:pt-24 sm:pb-40 lg:pt-40 lg:pb-48">
				<div className="relative max-w-7xl ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4 lg:px-6 sm:static">
					<div className={`sm:max-w-${width}`}>
						<h1 className="text-4xl font font-extrabold tracking-tight text-white sm:text-6xl">
							{title}
						</h1>
						<p className="mt-4 text-xl text-gray-200">
							{subtitle}
						</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Header;
