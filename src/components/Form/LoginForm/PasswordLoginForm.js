import React from "react";
import TextInput from "../../FormElements/TextInput";
import {LockClosedIcon, UserIcon} from "@heroicons/react/solid";
import ErrorList from "../../Error/ErrorList";
import SubmitButton from "../../FormElements/SubmitButton";

const PasswordLoginForm = ({loading, errors}) => {
	return (
		<>
			<div className="rounded-md shadow-sm -space-y-px">
				<div>
					<TextInput
						name="username"
						placeholder="Username"
						position="first"
						autoComplete="off"
						disabled={loading}
						icon={UserIcon}
						autoFocus={true}
					/>
				</div>
				<div>
					<TextInput
						name="password"
						placeholder="Password"
						position="last"
						autoComplete="off"
						disabled={loading}
						icon={LockClosedIcon}
						type="password"
					/>
				</div>
			</div>

			<ErrorList errors={errors}/>

			<SubmitButton
				text={loading ? "Signing In..." : "Sign In"}
				loading={loading}
			/>
		</>
	);
};

export default PasswordLoginForm;
