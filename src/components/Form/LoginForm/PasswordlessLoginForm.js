import React from "react";
import TextInput from "../../FormElements/TextInput";
import {UserIcon} from "@heroicons/react/solid";
import ErrorList from "../../Error/ErrorList";
import SubmitButton from "../../FormElements/SubmitButton";

const PasswordlessLoginForm = ({loading, errors}) => {
	return (
		<>
			<div className="rounded-md shadow-sm -space-y-px">
				<div>
					<TextInput
						name="username"
						placeholder="Username"
						position="alone"
						autoComplete="off"
						disabled={loading}
						icon={UserIcon}
						autoFocus={true}
					/>
				</div>
			</div>

			<ErrorList errors={errors}/>

			<SubmitButton
				text={loading ? "Signing In..." : "Sign In"}
				loading={loading}
			/>
		</>
	);
};

export default PasswordlessLoginForm;
