import React, {useRef, useState} from "react";
import TextInput from "../FormElements/TextInput";
import {
	ClipboardIcon,
	CodeIcon,
	PencilAltIcon,
	ViewListIcon
} from "@heroicons/react/solid";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import {useRouter} from "next/router";
import {mutate} from "swr";
import TextAreaInput from "../FormElements/TextAreaInput";
import Checkbox from "../FormElements/Checkbox";
import CreateProjectSchema from "../../schemas/Frontend/CreateProjectSchema";
import editProject from "../../helpers/apiMethods/projects/editProject";
import EditableSkillsList from "../Admin/Projects/EditableSkillsList";
import EditableContributorsList from "../Admin/Projects/EditableContributorsList";
import EditableImagesList from "../Admin/Projects/EditableImagesList";

const EditProjectForm = ({onChange, baseProject}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const selectedSkillIds = useRef(baseProject.skills.map(skill => skill.skillId));
	const addedContributors = useRef(baseProject.contributors);
	const addedImages = useRef(baseProject.imageUrls);

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		onChange({name, value});
	};

	const handleOnChecked = (event) => {
		let {name, checked} = event.target;

		onChange({name, value: checked});
	};

	const handleSkillsListChanged = (newSkills) => {
		selectedSkillIds.current = newSkills;
	};

	const handleContributorsChanged = (newContributors) => {
		addedContributors.current = newContributors;
	};

	const handleImagesListChanged = (newImages) => {
		addedImages.current = newImages;
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			name: nameElement,
			shortDescription: shortDescriptionElement,
			longDescription: longDescriptionElement,
			repository: repositoryElement,
			openSource: openSourceElement
		} = event.target;

		let name = nameElement.value;
		let shortDescription = shortDescriptionElement.value;
		let longDescription = longDescriptionElement.value;
		let repository = repositoryElement.value;
		let imageUrls = addedImages.current;
		let openSource = openSourceElement.checked;
		let contributors = addedContributors.current;
		let skillIds = selectedSkillIds.current;

		let validationResult = CreateProjectSchema.validate({
			name,
			shortDescription,
			longDescription,
			repository,
			openSource,
			imageUrls,
			contributors,
			skillIds
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			name,
			shortDescription,
			longDescription,
			repository,
			openSource,
			imageUrls,
			contributors,
			skillIds
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await editProject(baseProject.id, name, shortDescription,
			longDescription, openSource, repository, imageUrls,
			contributors, skillIds);

		if (error) {
			setLoading(false);
			setErrors([{message: error.displayMessage}]);
			return;
		}

		await mutate("/api/project");
		await mutate(`/api/project/${baseProject.id}`);

		setLoading(false);

		await router.push("/admin/projects?editedProject=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<TextInput
						name="name"
						placeholder="Name"
						position="alone"
						autoComplete="off"
						icon={PencilAltIcon}
						defaultValue={baseProject.name}
						onInput={handleOnInput}
					/>

					<TextInput
						name="shortDescription"
						placeholder="Short Description"
						position="alone"
						autoComplete="off"
						icon={ClipboardIcon}
						defaultValue={baseProject.shortDescription}
						onInput={handleOnInput}
					/>

					<TextAreaInput
						name="longDescription"
						placeholder="Long Description"
						position="alone"
						autoComplete="off"
						icon={ViewListIcon}
						onInput={handleOnInput}
						defaultValue={baseProject.longDescription}
						rows={5}
					/>

					<TextInput
						name="repository"
						placeholder="Repository"
						position="alone"
						autoComplete="off"
						icon={CodeIcon}
						defaultValue={baseProject.repository}
						onInput={handleOnInput}
					/>

					<EditableImagesList
						defaultImages={baseProject.imageUrls}
						onChange={handleImagesListChanged}
					/>

					<Checkbox
						name="openSource"
						text="Open Source"
						colour="indigo"
						checked={baseProject.openSource}
						onChecked={handleOnChecked}
					/>

					<EditableSkillsList
						defaultSkills={baseProject.skills.map(skill => skill.skillId)}
						onChange={handleSkillsListChanged}
					/>

					<EditableContributorsList
						defaultContributors={baseProject.contributors}
						onChange={handleContributorsChanged}
					/>

					<ErrorList errors={errors}/>

					<SubmitButton
						text={loading ? "Saving..." : "Save"}
						loading={loading}
					/>
				</form>
			</div>
		</div>
	);
};

export default EditProjectForm;
