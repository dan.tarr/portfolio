import React, {useState} from "react";
import TextInput from "../FormElements/TextInput";
import {
	BriefcaseIcon,
	LinkIcon,
	PencilAltIcon,
	PhotographIcon,
	ViewListIcon
} from "@heroicons/react/solid";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import {useRouter} from "next/router";
import TextAreaInput from "../FormElements/TextAreaInput";
import Checkbox from "../FormElements/Checkbox";
import DateInput from "../FormElements/DateInput";
import EditExperienceSchema from "../../schemas/API/EditExperienceSchema";
import editExperience from "../../helpers/apiMethods/experience/editExperience";

const EditExperienceForm = ({onChange, baseExperience}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const [pastExperience, setPastExperience] = useState(baseExperience.endDate !== null);
	const [startDate, setStartDate] = useState(0);
	const [endDate, setEndDate] = useState(0);

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		onChange({name, value});
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			title: titleElement,
			description: descriptionElement,
			company: companyElement,
			url: urlElement,
			iconUrl: iconUrlElement
		} = event.target;

		let title = titleElement.value;
		let description = descriptionElement.value;
		let company = companyElement.value;
		let url = urlElement.value;
		let iconUrl = iconUrlElement.value;

		let validationResult = EditExperienceSchema.validate({
			title,
			description,
			company,
			url,
			iconUrl,
			startDate,
			endDate: pastExperience ? endDate : null
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			title,
			description,
			company,
			url,
			iconUrl
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await editExperience(baseExperience.id, title, description, company,
			url, iconUrl, startDate, pastExperience ? endDate : null);

		setLoading(false);

		if (error) {
			setErrors([{message: error.displayMessage}]);
			return;
		}

		await router.push("/admin/experience?editedExperience=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<TextInput
						name="title"
						placeholder="Title"
						position="alone"
						autoComplete="off"
						icon={PencilAltIcon}
						defaultValue={baseExperience.title}
						onInput={handleOnInput}
					/>

					<TextAreaInput
						name="description"
						placeholder="Description"
						position="alone"
						autoComplete="off"
						icon={ViewListIcon}
						defaultValue={baseExperience.description}
						onInput={handleOnInput}
					/>

					<TextInput
						name="company"
						placeholder="Company"
						position="alone"
						autoComplete="off"
						icon={BriefcaseIcon}
						defaultValue={baseExperience.company}
						onInput={handleOnInput}
					/>

					<TextInput
						name="url"
						placeholder="URL"
						position="alone"
						autoComplete="off"
						icon={LinkIcon}
						defaultValue={baseExperience.url}
						onInput={handleOnInput}
					/>

					<TextInput
						name="iconUrl"
						placeholder="Icon URL"
						position="alone"
						autoComplete="off"
						icon={PhotographIcon}
						defaultValue={baseExperience.iconUrl}
						onInput={handleOnInput}
					/>

					{/* TODO: Prefill existing dates */}

					<DateInput
						text="Start Date"
						onChange={(newObj) => {
							onChange({
								name: "startDate",
								value: newObj.timestamp
							});
							setStartDate(newObj.timestamp);
						}}
					/>

					<Checkbox
						name="pastExperience"
						text="Past Experience"
						onChecked={({target}) => {
							setPastExperience(target.checked);

							if (!target.checked)
								onChange({name: "endDate", value: null});
							else
								onChange({name: "endDate", value: endDate});
						}}
						checked={pastExperience}
					/>

					<DateInput
						text="End Date"
						onChange={(newObj) => {
							onChange({
								name: "endDate",
								value: newObj.timestamp
							});
							setEndDate(newObj.timestamp);
						}}
						disabled={!pastExperience}
					/>

					<ErrorList errors={errors}/>

					<SubmitButton
						text={loading ? "Saving..." : "Save"}
						loading={loading}
					/>
				</form>
			</div>
		</div>
	);
};

export default EditExperienceForm;
