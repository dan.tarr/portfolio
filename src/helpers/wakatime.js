import {getJson, setexJson} from "./redis";
import axios from "axios";
import {DateTime, Duration} from "luxon";

export const getStatistics = async () => {
	let cachedStats = await getJson("wakatimeStatistics");

	// Don't use cached stats for dev
	if (cachedStats && process.env.ENVIRONMENT !== "dev")
		return cachedStats;

	let base64ApiKey = Buffer.from(process.env.WAKATIME_API_KEY).toString("base64");

	let [
		weekStatsResponse,
		monthStatsResponse,
		allTimeStatsResponse,
		leaderboardResponse
	] = await Promise.all([
		axios.get("https://wakatime.com/api/v1/users/current/summaries?range=Last%207%20Days", {
			headers: {
				authorization: `Basic ${base64ApiKey}`
			}
		}),
		axios.get("https://wakatime.com/api/v1/users/current/summaries?range=Last%2030%20Days", {
			headers: {
				authorization: `Basic ${base64ApiKey}`
			}
		}),
		axios.get("https://wakatime.com/api/v1/users/current/stats/all_time", {
			headers: {
				authorization: `Basic ${base64ApiKey}`
			}
		}),
		axios.get("https://wakatime.com/api/v1/leaders", {
			headers: {
				authorization: `Basic ${base64ApiKey}`
			}
		})
	]);

	let monthStats = monthStatsResponse.data.data.map(day => {
		return {
			text: day.grand_total.text,
			seconds: day.grand_total.total_seconds,
			date: day.range.date
		};
	});

	let statsObject = {
		lastWeekTotal: weekStatsResponse.data.cummulative_total.text,
		month: monthStats,
		bestDay: {
			date: allTimeStatsResponse.data.data.best_day.date,
			text: allTimeStatsResponse.data.data.best_day.text
		},
		leaderboard: {
			rank: leaderboardResponse.data.current_user.rank
		}
	};

	await setexJson("wakatimeStatistics", statsObject, 900);

	return statsObject;
};

export const getLastActivity = async () => {
	let base64ApiKey = Buffer.from(process.env.WAKATIME_API_KEY).toString("base64");

	let currentDate = DateTime.now();
	let lastRecordedActivity = null;

	// Go up to 7 days back to find the latest activity
	for (let i = 0; i < 7; i++) {
		let currentDateString = currentDate.toFormat("yyyy-MM-dd");
		// eslint-disable-next-line max-len
		let url = `https://wakatime.com/api/v1/users/current/durations?date=${currentDateString}&slice_by=editor`;

		let response = (await axios.get(url, {
			headers: {
				authorization: `Basic ${base64ApiKey}`
			}
		})).data;

		let {data: dataPoints} = response;

		if (dataPoints.length > 0) {
			let lastPoint = dataPoints[dataPoints.length - 1];
			let timeMs = Math.round(lastPoint.time * 1000);
			let durationMs = Math.round(lastPoint.duration * 1000);

			lastRecordedActivity = {
				timestamp: timeMs + durationMs,
				editor: lastPoint.editor
			};
			break;
		}

		currentDate = currentDate.minus(Duration.fromObject({
			days: 1
		}));
	}

	return lastRecordedActivity;
};
