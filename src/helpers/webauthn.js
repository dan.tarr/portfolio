import crypto from "crypto";
import base64url from "base64url";
import cbor from "cbor";

export const randomBase64URLString = (length) => {
	return base64url(crypto.randomBytes(length));
};

export const generateServerChallenge = (id, username) => {
	return {
		challenge: randomBase64URLString(32),
		rp: {
			name: "Portfolio",
			id: process.env.WEBAUTHN_RP_ID
		},
		user: {
			id,
			name: username,
			displayName: username
		},
		assestation: "direct",
		pubKeyCredParams: [
			{
				type: "public-key",
				alg: -7
			},
			{
				type: "public-key",
				alg: -35
			},
			{
				type: "public-key",
				alg: -36
			},
			{
				type: "public-key",
				alg: -257
			},
			{
				type: "public-key",
				alg: -258
			},
			{
				type: "public-key",
				alg: -259
			},
			{
				type: "public-key",
				alg: -38
			},
			{
				type: "public-key",
				alg: -39
			},
			{
				type: "public-key",
				alg: -8
			}
		],
		authenticatorSelection: {
			requireResidentKey: false,
			userVerification: "discouraged"
		}
	};
};

export const generateAssertionChallenge = (id) => {
	return {
		challenge: randomBase64URLString(32),
		allowCredentials: [
			{
				type: "public-key",
				id,
				transports: [
					"usb",
					"nfc",
					"ble",
					"internal"
				]
			}
		]
	};
};

export const parseMakeCredAuthData = (inputBuffer) => {
	let buffer = inputBuffer;

	const rpIdHash = buffer.slice(0, 32);
	buffer = buffer.slice(32);

	const flagsBuf = buffer.slice(0, 1);
	buffer = buffer.slice(1);

	const flags = flagsBuf[0];

	const counterBuf = buffer.slice(0, 4);
	buffer = buffer.slice(4);

	const counter = counterBuf.readUInt32BE(0);

	const aaguid = buffer.slice(0, 16);
	buffer = buffer.slice(16);

	const credIDLenBuf = buffer.slice(0, 2);
	buffer = buffer.slice(2);

	const credIDLen = credIDLenBuf.readUInt16BE(0);

	const credID = buffer.slice(0, credIDLen);
	buffer = buffer.slice(credIDLen);

	const COSEPublicKey = buffer;

	return { rpIdHash, flagsBuf, flags, counter, counterBuf, aaguid, credID, COSEPublicKey };
};

export const parseGetAssertAuthData = (inputBuffer) => {
	let buffer = inputBuffer;
	const rpIdHash = buffer.slice(0, 32);
	buffer = buffer.slice(32);

	const flagsBuf = buffer.slice(0, 1);
	buffer = buffer.slice(1);

	const flags = flagsBuf[0];

	const counterBuf = buffer.slice(0, 4);

	const counter = counterBuf.readUInt32BE(0);

	return { rpIdHash, flagsBuf, flags, counter, counterBuf };
};

export const COSEECDHAtoPKCS = async (COSEPublicKey) => {
	let [coseStruct] = await cbor.decodeAll(COSEPublicKey);
	let tag = Buffer.from([0x04]);

	let x = coseStruct.get(-2);
	let y = coseStruct.get(-3);

	return Buffer.concat([tag, x, y]);
};

export const ASN1toPEM = async (inputPkBuffer) => {
	let type;
	let pkBuffer = inputPkBuffer;

	if (pkBuffer.length === 65 && pkBuffer[0] === 0x04) {
		pkBuffer = Buffer.concat([
			Buffer.from("3059301306072a8648ce3d020106082a8648ce3d030107034200", "hex"),
			pkBuffer
		]);

		type = "PUBLIC KEY";
	} else
		type = "CERTIFICATE";

	let b64cert = pkBuffer.toString("base64");

	let PEMKey = "";

	for (let i = 0; i < Math.ceil(b64cert.length / 64); i++) {
		let start = 64 * i;

		PEMKey += b64cert.substr(start, 64) + "\n";
	}

	return `-----BEGIN ${type}-----\n` + PEMKey + `-----END ${type}-----\n`;
};

export const verifyAuthenticatorAttestationResponse = async (webauthnResponse) => {
	let attestationBuffer = base64url.toBuffer(webauthnResponse.attestationObject);
	let [ctapMakeCredResp] = await cbor.decodeAll(attestationBuffer);

	let authrDataStruct = parseMakeCredAuthData(ctapMakeCredResp.authData);

	let publicKey = await COSEECDHAtoPKCS(authrDataStruct.COSEPublicKey);

	let response = {verified: false};

	if (ctapMakeCredResp.fmt === "none")
		response.verified = true;

	if (response.verified) {
		response.authrInfo = {
			fmt: "fido-u2f",
			publicKey: base64url.encode(publicKey),
			counter: authrDataStruct.counter,
			credID: base64url.encode(authrDataStruct.credID)
		};
	}

	return response;
};

export const hash = (data) => {
	return crypto.createHash("sha256")
		.update(data)
		.digest();
};

export const verifySignature = (signature, data, publicKey) => {
	return crypto.createVerify("SHA256")
		.update(data)
		.verify(publicKey, signature);
};

export const verifyAuthenticatorAssertionResponse = async (webauthnResponse, authr) => {
	let authenticatorData = base64url.toBuffer(webauthnResponse.authenticatorData);

	let response = {verified: false};

	if (["fido-u2f"].includes(authr.fmt)) {
		let authrDataStruct = parseGetAssertAuthData(authenticatorData);

		let clientDataHash = hash(base64url.toBuffer(webauthnResponse.clientDataJSON));
		let signatureBase = Buffer.concat([authenticatorData, clientDataHash]);

		let publicKey = await ASN1toPEM(base64url.toBuffer(authr.publicKey));
		let signature = base64url.toBuffer(webauthnResponse.signature);

		response.counter = authrDataStruct.counter;
		response.verified = verifySignature(signature, signatureBase, publicKey + "d");
	}

	return response;
};

export const publicKeyCredentialToJson = (pubKeyCred) => {
	if (pubKeyCred instanceof Array) {
		let arr = [];
		for (let i of pubKeyCred) arr.push(publicKeyCredentialToJson(i));

		return arr;
	} else if (pubKeyCred instanceof ArrayBuffer)
		return Buffer.from(pubKeyCred).toString("base64");
	else if (pubKeyCred instanceof Object) {
		let obj = {};

		// eslint-disable-next-line guard-for-in
		for (let key in pubKeyCred)
			// noinspection JSUnfilteredForInLoop
			obj[key] = publicKeyCredentialToJson(pubKeyCred[key]);

		return obj;
	}

	return pubKeyCred;
};
