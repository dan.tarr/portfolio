import axios from "../../axiosWithRefreshToken";
import wrapAxiosRequest from "../../wrapAxiosRequest";

const loginResponse = async (body) => {
	return wrapAxiosRequest(axios.post("/api/auth/webauthn/loginResponse", body));
};

export default loginResponse;
