import axios from "../../axiosWithRefreshToken";
import wrapAxiosRequest from "../../wrapAxiosRequest";

const initialiseRegistration = async () => {
	return wrapAxiosRequest(axios.post("/api/auth/webauthn/register/initialise"));
};

export default initialiseRegistration;
