import axios from "../../axiosWithRefreshToken";
import wrapAxiosRequest from "../../wrapAxiosRequest";

const remove = async () => {
	return wrapAxiosRequest(axios.delete("/api/auth/webauthn/remove"));
};

export default remove;
