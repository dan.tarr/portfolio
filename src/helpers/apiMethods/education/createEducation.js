import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const createEducation = async (subject, level, location, grade, startDate, endDate) => {
	return wrapAxiosRequest(axios.post("/api/education", {
		subject,
		level,
		location,
		grade,
		startDate,
		endDate
	}));
};

export default createEducation;
