const WrapAxiosRequest = async (axiosRequest) => {
	try {
		let response = await axiosRequest;

		return {
			error: null,
			body: response.data,
			headers: response.headers,
			status: response.status
		};
	} catch (err) {
		let response = err.response;

		return {
			error: response.data,
			body: null,
			headers: response.headers,
			status: response.status
		};
	}
};

export default WrapAxiosRequest;
