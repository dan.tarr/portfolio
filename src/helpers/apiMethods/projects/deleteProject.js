import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const deleteProject = async (projectId) => {
	return wrapAxiosRequest(axios.delete(`/api/project/${projectId}`));
};

export default deleteProject;
