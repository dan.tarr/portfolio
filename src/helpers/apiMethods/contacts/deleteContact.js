import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const deleteContact = async (contactId) => {
	return wrapAxiosRequest(axios.delete(`/api/contact/${contactId}`));
};

export default deleteContact;
