import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const createContact = async (title, text, link, baseSiteLink, icon, iconColour) => {
	return wrapAxiosRequest(axios.post("/api/contact", {
		title,
		text,
		link,
		baseSiteLink,
		icon,
		iconColour
	}));
};

export default createContact;
