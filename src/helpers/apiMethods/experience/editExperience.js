import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const editExperience =
	async (id, title, description, company, url, iconUrl, startDate, endDate) => {
		return wrapAxiosRequest(axios.put(`/api/experience/${id}`, {
			title,
			description,
			company,
			url,
			iconUrl,
			startDate,
			endDate
		}));
	};

export default editExperience;
