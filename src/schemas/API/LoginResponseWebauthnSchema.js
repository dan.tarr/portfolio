import Joi from "joi";

export default Joi.object({
	id: Joi.required(),
	rawId: Joi.binary()
		.required(),
	response: Joi.object({
		authenticatorData: Joi.string()
			.required(),
		signature: Joi.string()
			.required(),
		userHandle: Joi.string()
			.allow(null)
			.required(),
		clientDataJSON: Joi.string()
			.required()
	}).required(),
	type: Joi.string()
		.valid("public-key")
		.required(),
	username: Joi.string()
		.required()
});
