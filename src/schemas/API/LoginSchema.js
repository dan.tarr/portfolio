import Joi from "joi";

export default Joi.object({
	username: Joi.string()
		.max(128)
		.required(),
	password: Joi.string()
		.required()
});
