import Joi from "joi";
import Contributors from "./Contributors";

export default Joi.object({
	name: Joi.string()
		.max(128)
		.required(),
	shortDescription: Joi.string()
		.max(256)
		.required(),
	longDescription: Joi.string()
		.max(16777215)
		.required(),
	openSource: Joi.boolean()
		.required(),
	repository: Joi.string()
		.uri({
			domain: {
				tlds: ["com"]
			},
			scheme: "https"
		})
		.allow(null)
		.max(1024)
		.required(),
	contributors: Joi.array()
		.items(Contributors)
		.required(),
	imageUrls: Joi.array()
		.items(Joi.string()
			.uri({
				scheme: "https"
			})
			.allow(null)
			.max(512)
		)
		.unique()
		.required(),
	skillIds: Joi.array()
		.items(Joi.string()
			.length(22)
		)
		.unique()
		.required()
});
