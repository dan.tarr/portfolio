import Joi from "joi";

export default Joi.object({
	id: Joi.required(),
	rawId: Joi.binary()
		.required(),
	response: Joi.object({
		attestationObject: Joi.string()
			.required(),
		clientDataJSON: Joi.string()
			.base64()
			.required()
	}).required(),
	type: Joi.string()
		.valid("public-key")
		.required()
});
