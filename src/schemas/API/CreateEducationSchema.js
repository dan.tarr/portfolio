import Joi from "joi";

export default Joi.object({
	location: Joi.string()
		.max(128)
		.required(),
	level: Joi.string()
		.max(64)
		.required(),
	subject: Joi.string()
		.max(64)
		.required(),
	startDate: Joi.number()
		.min(1900)
		.max(2100)
		.required(),
	endDate: Joi.number()
		.min(1900)
		.max(2100)
		.greater(Joi.ref("startDate", {
			render: true
		})),
	grade: Joi.string()
		.max(32)
		.required()
});
