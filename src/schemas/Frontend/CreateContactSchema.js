import Joi from "joi";

export default Joi.object({
	title: Joi.string()
		.max(64)
		.required()
		.label("Title"),
	text: Joi.string()
		.max(512)
		.required()
		.label("Text"),
	link: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(1024)
		.allow(null)
		.required()
		.label("Link"),
	baseSiteLink: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(512)
		.required()
		.label("Base Site Link"),
	icon: Joi.string()
		.max(128)
		.label("Icon"),
	iconColour: Joi.when("icon", {
		is: Joi.exist(),
		then: Joi.string()
			.length(6)
			.hex()
			.required()
			.label("Icon Colour")
	})
});
