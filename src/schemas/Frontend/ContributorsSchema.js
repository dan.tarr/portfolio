import Joi from "joi";

export default Joi.object({
	name: Joi.string()
		.max(128)
		.label("Name")
		.required(),
	link: Joi.string()
		.max(1024)
		.uri({
			scheme: [
				"http",
				"https"
			]
		})
		.label("Link")
		.required()
});
