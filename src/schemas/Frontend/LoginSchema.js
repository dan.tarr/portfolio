import Joi from "joi";

export default Joi.object({
	username: Joi.string()
		.max(128)
		.required()
		.label("Username"),
	password: Joi.string()
		.required()
		.label("Password")
});
