const {generateKeyPair} = require("crypto");
const fs = require("fs/promises");
const prompts = require("prompts");

const generateKeyPairAsync = (keyLength) => {
	return new Promise((resolve) => {
		generateKeyPair("rsa", {
			modulusLength: keyLength,
			publicKeyEncoding: {
				type: "spki",
				format: "pem"
			},
			privateKeyEncoding: {
				type: "pkcs8",
				format: "pem"
			}
		}, ((err, publicKey, privateKey) => {
			if (err) {
				console.error(err);
				process.exit(1);
			}

			return resolve({
				publicKey,
				privateKey
			});
		}));
	});
};

(async () => {
	let {keyLength, outputLocation, envFileName} = await prompts([
		{
			type: "number",
			name: "keyLength",
			message: "Key length",
			initial: 2048,
			validate: value => {
				if (value < 512)
					return "Key length must be greater than or equal to 512";

				return true;
			}
		},
		{
			type: "select",
			name: "outputLocation",
			message: "Output location?",
			choices: [
				{title: "Console", value: "console"},
				{title: "Environment Variable File", value: "envFile"}
			]
		},
		{
			type: prev => prev === "envFile" ? "text" : null,
			name: "envFileName",
			message: "Environment Variable Filename",
			initial: ".env.local"
		}
	]);

	console.log("Generating key pair...");

	let {publicKey, privateKey} = await generateKeyPairAsync(keyLength);

	console.log("Successfully generated key pair!");

	if (outputLocation === "console") {
		console.log(publicKey, privateKey);
		process.exit(0);
	}

	if (outputLocation === "envFile") {
		let fileContents;

		try {
			fileContents = await fs.readFile(`./${envFileName}`, "utf8");
		} catch (err) {
			fileContents = "";
		}

		let fileLines = fileContents.split("\r\n");

		let existingPrivateKeyLine = fileLines.findIndex(text => text.startsWith("JWT_PRIVATE_KEY="));
		let existingPublicKeyLine = fileLines.findIndex(text => text.startsWith("JWT_PUBLIC_KEY="));

		if (existingPrivateKeyLine === -1) {
			fileLines.push(`JWT_PRIVATE_KEY="${publicKey.replace(/\n/g, "\\n")}"`);
			fileLines.push("");
		} else
			fileLines[existingPrivateKeyLine] = `JWT_PRIVATE_KEY="${privateKey.replace(/\n/g, "\\n")}"`;

		if (existingPublicKeyLine === -1) {
			fileLines.push(`JWT_PUBLIC_KEY="${publicKey.replace(/\n/g, "\\n")}"`);
			fileLines.push("");
		} else
			fileLines[existingPublicKeyLine] = `JWT_PUBLIC_KEY="${publicKey.replace(/\n/g, "\\n")}"`;

		await fs.writeFile(`./${envFileName}`, fileLines.join("\r\n"));
		process.exit(0);
	}
})();
